import React, {Component} from 'react';
import './App.scss';
import {BrowserRouter, Route} from "react-router-dom";
import FlowersList from './components/FlowersList/FlowersList'
import CompareWithContainer from "./components/CompareWith/CompareWithContainer";
import NavbarContainer from "./components/Navbar/NavbarContainer";
import {initilizedSuccess} from "./redux/appReducer";
import {connect} from 'react-redux'
import CreateFlowersContainer from './components/CreateFLowers/CreateFLowersContainer'
import CartContainer from "./components/Cart/CartContainer";
import SympathyContainer from "./components/Sympathy/SympathyContainer";
import SignUpContainer from "./components/SignUp/SignUpContainer";
import SignInContainer from "./components/SignIn/SignInContainer";
import ProfileContainer from "./components/Profile/ProfileContainer";
import Preloader from "./components/common/Preloader/Preloader";
import CardContainer from "./components/Card/CardContainer";
import TrackOrderContainer from "./components/TrackOrder/TrackOrderContainer";
import OrdersControlContainer from "./components/OrdersControl/OrdersControlContainer";
import CatalogContainer from "./components/Catalog/CatalogContainer";


class App extends Component {

    componentDidMount() {
        this.props.initilizedSuccess()
    }

    render() {
        if (this.props.Initialized) {
            return (
                <BrowserRouter>
                    <div className="App">
                        <NavbarContainer/>
                        <Route exact path='/' render={() => <FlowersList/>}/>
                        <Route exact path='/signup' render={() => <SignUpContainer/>}/>
                        <Route exact path='/signin' render={() => <SignInContainer/>}/>
                        <Route exact path='/trackorder' render={() => <TrackOrderContainer />} />
                        <Route exact path='/profile' render={() => <ProfileContainer/>}/>
                        <Route path='/cart/:id' render={() => <CartContainer />}/>
                        <Route path='/card' render={() => <CardContainer />}/>
                        <Route path='/compare' render={() => <CompareWithContainer/>}/>
                        <Route path='/catalog/:params' render={() => <CatalogContainer/>}/>
                        <Route exact path='/catalog' render={() => <CatalogContainer/>}/>
                        <Route path='/sympathy' render={() => <SympathyContainer />}/>
                        <Route path='/admin/create' render={() => <CreateFlowersContainer/>}/>
                        <Route path='/admin/orders' render={() => <OrdersControlContainer/>}/>
                    </div>
                </BrowserRouter>
            )
        }else{
            return <Preloader />
        }
    }
}

const mapStateToProps = (state) => {
    return {
        Initialized: state.app.Initialized
    }
}

export default connect(mapStateToProps, {initilizedSuccess})(App);
