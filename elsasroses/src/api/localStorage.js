export const newArrayWithStorage = (flowerId, typeStorage, count = 1, isCart, filterTitle) => {
    let newDataCompareId = [...[]];
    let sameId = false;

    let dataCompareId = JSON.parse(window.localStorage.getItem(typeStorage));
    if (dataCompareId.length !== 0) {
        newDataCompareId = dataCompareId.filter((id) => {
            if (typeStorage === 'card') {
                let idCount = id.split('_');
                if (flowerId !== parseInt(idCount[0])){
                    return id;
                }else if(isCart) sameId = false;
                else sameId = true;
            } else {
                if (flowerId !== id) return id;
                else sameId = true;
            }
        });
        if (!sameId) {
            if (typeStorage === 'card') newDataCompareId.push(`${flowerId}_${count}_|${filterTitle}|`); // для корзины - указываем количество товаров еще
            else newDataCompareId.push(flowerId);
        }
    } else {
        if (typeStorage === 'card') newDataCompareId.push(`${flowerId}_${count}_|${filterTitle}|`);
        else newDataCompareId.push(flowerId);
    }


    window.localStorage.setItem(typeStorage, JSON.stringify(newDataCompareId));
    if (newDataCompareId.length === 0) return;


    return newDataCompareId;
}

export const deleteFromStorage = (flowerId, typeStorage) => {
    let storage = localStorage.getItem(typeStorage);
    let flowersIdStorage = JSON.parse(storage);

    let newStorageData =  flowersIdStorage.filter(id => {
        if(flowerId === 'all') return false;
        let checkFlowerId;
        if(typeStorage === 'card'){
            let flowerIdCard = id.split('_');
            checkFlowerId = parseInt(flowerIdCard[0])
        }else{
            checkFlowerId = id;
        }
        if(checkFlowerId !== flowerId) return true;

    });
    localStorage.setItem(typeStorage, JSON.stringify(newStorageData));
}

export const setAuthUser = (userName) => {
    localStorage.setItem('user', userName);
}

export const deleteAuthUser = () => {
    localStorage.setItem('user', '');
}

export const getCardStorage = () => {
    let storage = localStorage.getItem('card');
    let formatStorage = JSON.parse(storage);
    return formatStorage.map(f => {
        let flowersId = f.split('_');
        return {id: flowersId[0], count: flowersId[1]};
    })
}