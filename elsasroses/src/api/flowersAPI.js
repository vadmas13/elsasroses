import firebase from './../config/fbConfig'
import {addFlowersBadges, addFlowerState} from "../redux/flowersReducer";
import {changeLoadingGetFlowers, changeLoadingGetParams, getFlowersApi, getFlowersParams} from "../redux/apiReducer";
import {addFlowersBadgesCompare, getParamsForFlower, newFlowerForCompare} from "../redux/flowerCompareReducer";
import {getOrders, loadedOrders} from "../redux/profileReducer";
import { getColorsBase} from "../redux/catalogReducer";


const COMPARE = 'compare', SYMPATHY = 'sympathy';

const db = firebase.firestore();

export const getLastFlowersAPI = () => (dispatch) => {
    db.collection('flowers').get().then((querySnapshot) => {
        dispatch(changeLoadingGetFlowers());
        querySnapshot.forEach((doc) => {
            let flowerData = doc.data();
            dispatch(addFlowerState(flowerData));
        })
    }).then(() => {
        db.collection('badges').get().then((querySnapshot) => {
            dispatch(changeLoadingGetFlowers());
            querySnapshot.forEach((doc) => {
                let flowerData = doc.data();
                dispatch(addFlowersBadges(flowerData));
            })
        })
    }).then(() => {
        dispatch(getFlowersApi());
        dispatch(changeLoadingGetFlowers());
    })
}

export const getFlowersColors = (useColors) => (dispatch) => {
    db.collection('flowersColors').doc('default').get().then((querySnapshot) => {
        let flowerData = querySnapshot.data();
        dispatch(getColorsBase(flowerData.colors, useColors));
    })
}

export const getFlowersIdCompareOrSympathy = (typeStorage) => (dispatch) => {
    dispatch(changeLoadingGetFlowers());
    let storageData = localStorage.getItem(typeStorage);
    let flowersId = JSON.parse(storageData);
    if (flowersId.length !== 0) {
        flowersId.forEach((id, i) => {
            db.collection('flowers').where('id', '==', id).get().then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                    let flowerData = doc.data();
                    if (typeStorage === COMPARE) dispatch(newFlowerForCompare(flowerData));
                    else if (typeStorage === SYMPATHY) dispatch(addFlowerState(flowerData));
                })
            }).then(() => {
                db.collection('badges').where('id', '==', id).get().then((querySnapshot) => {
                    querySnapshot.forEach((doc) => {
                        let flowerData = doc.data();
                        if (typeStorage === COMPARE) dispatch(addFlowersBadgesCompare(flowerData));
                        else if (typeStorage === SYMPATHY) dispatch(addFlowersBadges(flowerData));
                    })
                })
            }).then(() => {
                if (flowersId.length - 1 === i) {
                    dispatch(getFlowersApi());
                    dispatch(changeLoadingGetFlowers());
                }
            })
        })
    } else {
        dispatch(getFlowersApi());
    }
}

export const getFlowersParamsForCompare = () => (dispatch) => {
    dispatch(changeLoadingGetParams());
    let storageData = localStorage.getItem('compare');
    let flowersId = JSON.parse(storageData);
    if (flowersId.length !== 0) {
        flowersId.forEach((id, i) => {
            db.collection('params').where('id', '==', id).get().then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                    let flowerData = doc.data();
                    dispatch(getParamsForFlower(flowerData));
                })
            }).then(() => {
                if (flowersId.length - 1 === i) {
                    dispatch(getFlowersParams());
                    dispatch(changeLoadingGetParams());
                }
            })
        })
    } else {
        dispatch(getFlowersParams());
    }
}


export const getTrackNumber = (trackNumber) => (dispatch) => {
    db.collection('orders').where('trackNumber', '==', trackNumber).get()
        .then((querySnapshot) => {
            if (querySnapshot.docs.length !== 0) {
                let trackOrderDoc = querySnapshot.docs[0];
                let trackOrder = trackOrderDoc.data();

                dispatch({type: 'GET_ORDER_DATA', trackOrder})
            } else {
                dispatch({type: 'GET_ORDER_DATA_EMPTY'})
            }
        })
}

export const getOrdersToUser = (uid) => (dispatch) => {
    db.collection('orders').where('user', '==', uid).get()
        .then((querySnapshot) => {
            querySnapshot.docs.map(order => {
                let data = order.data();
                dispatch(getOrders(data));
            });
        }).then(() => dispatch(loadedOrders()))
}





