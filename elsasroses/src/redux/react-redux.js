import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import flowerCompareReducer from "./flowerCompareReducer";
import thunkMiddleware from "redux-thunk"
import flowersReducer from "./flowersReducer";
import navbarReducer from "./navbarReducer";
import appReducer from "./appReducer";
import {reactReduxFirebase, getFirebase} from 'react-redux-firebase';
import {reduxFirestore, getFirestore} from 'redux-firestore';
import fbConfig from "./../config/fbConfig";
import {firestoreReducer} from "redux-firestore";
import {firebaseReducer} from "react-redux-firebase";
import flowerCreaterReducer from "./createFlowerReducer";
import categoryReducer from "./categoryReducer";
import cartReducer from "./cartReducer";
import apiReducer from "./apiReducer";
import flowerSympathyReducer from "./flowerSympathyReducer";
import authReducer from "./authReducer";
import orderCardReducer from "./orderCardReducer";
import trackOrderReducer from "./trackOrderReducer";
import profileReducer from "./profileReducer";
import ordersControlReducer from "./ordersControlReducer";
import catalogReducer from "./catalogReducer";
import addReviewsReducer from "./addReviewsReducer";
import reviewsListReducer from "./reviewsListReducer";


let reducers = combineReducers({
    compare:flowerCompareReducer,
    sympathy: flowerSympathyReducer,
    flowers: flowersReducer,
    navbar: navbarReducer,
    app: appReducer,
    createFlower: flowerCreaterReducer,
    categories: categoryReducer,
    cart: cartReducer,
    trackOrder: trackOrderReducer,
    orderCard: orderCardReducer,
    auth: authReducer,
    ordersControl: ordersControlReducer,
    catalog: catalogReducer,
    reviewsList: reviewsListReducer,
    api: apiReducer,
    newReview: addReviewsReducer,
    profile: profileReducer,
    firestore: firestoreReducer,
    firebase: firebaseReducer
})

const store = createStore(reducers,  compose(
    applyMiddleware(thunkMiddleware.withExtraArgument({getFirebase, getFirestore})),
    reduxFirestore(fbConfig), // redux bindings for firestore
    reactReduxFirebase(fbConfig,
        { useFirestoreForProfile: true,
            userProfile: 'users',
            attachAuthIsReady: true}), // redux binding for firebase
));

export default store;