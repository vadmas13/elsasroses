
const GET_FLOWERS_API = 'GET_FLOWERS_API';
const GET_BUTTONS_VALUES = 'GET_BUTTONS_VALUES';
const CHANGE_LOADING_FLOWERS = 'CHANGE_LOADING_FLOWERS';
const CHANGE_LOADING_PARAMS = 'CHANGE_LOADING_PARAMS';
const GET_FLOWERS_PARAMS = 'GET_FLOWERS_PARAMS';
const SET_INITIAL_STATE = 'SET_INITIAL_STATE';


const initState = {
    getFlowers: false,
    loadingFlowers: false,
    getButtonsValues: false,
    loadingParamsForCompare: false,
    getParamsForCompare: false
}

const apiReducer = (state = initState, action) => {
    switch (action.type){
        case GET_FLOWERS_API:
            return {...state, getFlowers: true}
        case GET_BUTTONS_VALUES:
            return {...state, getButtonsValues: true}
        case CHANGE_LOADING_FLOWERS:
            return {...state, loadingFlowers: action.bool? action.bool : !state.loadingFlowers}
        case CHANGE_LOADING_PARAMS:
            return {...state, loadingParamsForCompare: !state.loadingParamsForCompare}
        case GET_FLOWERS_PARAMS:
            return {...state, getParamsForCompare: true}
        case SET_INITIAL_STATE:
            return {...initState}
        default: return state;
    }
}

export const getFlowersApi = () => (dispatch) => {
    dispatch({type: GET_FLOWERS_API})
}

export const setInitialState = () => (dispatch) => {
    dispatch({type: SET_INITIAL_STATE})
}

export const getButtonsValues = () => (dispatch) => {
    dispatch({type: GET_BUTTONS_VALUES})
}

export const changeLoadingGetFlowers = (bool) => (dispatch) => {
    dispatch({type: CHANGE_LOADING_FLOWERS, bool})
}

export const changeLoadingGetParams = () => (dispatch) => {
    dispatch({type: CHANGE_LOADING_PARAMS})
}

export const getFlowersParams = () => (dispatch) => {
    dispatch({type: GET_FLOWERS_PARAMS})
}

export default apiReducer;