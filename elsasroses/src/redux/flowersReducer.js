import {
    addFlower, addFlowersArrayCatalog,
    addNewFlower,
    changeButton,
    checkButtons, clearFlowers,
    deleteFlowerCart,
    flowerBadge
} from "./actions/actionsFlowers";
import {getButtonsValues} from "./apiReducer";
import {deleteFromStorage} from "../api/localStorage";


const initState = [
    /*{
        id: 5,
        title: 'Grand Prix Rose Hand tied',
        img: 'https://ld-prestashop.template-help.com/prestashop_62258/img/p/1/1/7/117-tm_home_default.jpg',
        rate: 2,
        price: 2400
    },
    {
        id: 2,
        title: 'Grand Prix Rose Hand tied',
        img: 'https://ld-prestashop.template-help.com/prestashop_62258/img/p/1/1/7/117-tm_home_default.jpg',
        rate: 2,
        price: 2400
    },
    {
        id: 4,
        title: 'Grand Prix Rose Hand tied',
        img: 'https://ld-prestashop.template-help.com/prestashop_62258/img/p/1/1/7/117-tm_home_default.jpg',
        rate: 2,
        price: 2400
    }*/
]

const flowersReducer = (state = initState, action) => {
    switch(action.type){
        case 'CHANGE_BUTTON': {
            let newState = state.map(f => {
                if(f.id === parseInt(action.data.flowerId)){
                    return{
                        ...f, buttons: {...f.buttons, [action.data.type]: !f.buttons[action.data.type]}
                    }
                }else{
                    return{
                        ...f, buttons: {...f.buttons}
                    }
                }
            });

            return [...newState]
        }
        case 'CHECK_BUTTONS':{
            let newState = state.map(f => {
                let buttons = {};
                action.values.map((val) => {
                    let checkedId = val.flowersId !== undefined ? val.flowersId.filter(id => {
                        let idStr = id + '';
                        let countsId = idStr.split('_');
                        if(countsId.length > 1 && parseInt(countsId[0]) === f.id) return countsId[0];
                        else if(id === f.id) return id;
                    }) : '';
                    let booleanCheckedId = !!checkedId.length;
                    buttons =  {...buttons, [val.name]: booleanCheckedId};
                    return buttons;
                })
                return {
                    ...f, buttons
                }
            });
            return [...newState]

        }
        case 'ADD_FLOWER':
            return [...state, action.flower]
        case 'ADD_FLOWERS_ARRAY':
            return [...action.flowers]
        case 'ADD_FLOWERS_BADGES':
            return [...state.map(flower => {
                if(flower && flower.id === action.badge.id){ return {...flower, badges: [...action.badge.names]} }
                else return {...flower}
            })]
        case 'DELETE_FLOWER_FROM_STATE':
            return [...state.filter(f => f.id !== action.flowerId)]

        case 'CLEAR_FLOWERS_STATE':
            return [];
        default: return state;
    }
}

export const changeValueOfButton = (data) => (dispatch) => {
    dispatch(changeButton({...data}));
}

export const checkValuesOfButtons = (values) => (dispatch) => {
    dispatch(checkButtons(values));
    dispatch(getButtonsValues())
}

export const deleteFlowerFromState = (flowerId, typeStorage) => (dispatch) =>{
    deleteFromStorage(flowerId, typeStorage);
    dispatch(deleteFlowerCart(flowerId))
}

export const addFlowerState = (flower) => (dispatch) => {
    dispatch(addFlower(flower));
}

export const clearFlowersState = () => (dispatch) => {
    dispatch(clearFlowers())
}

export const addFlowersBadges = (badge) => (dispatch) =>{
    dispatch(flowerBadge(badge));
}

export const addFlowersArray = (flowers) => (dispatch) => {
    dispatch(addFlowersArrayCatalog(flowers))
}



export default flowersReducer;

