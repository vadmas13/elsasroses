
import {deleteFromStorage} from "../api/localStorage";
import {addBadgeSympathy, addSympathyFlowers, deleteFlowerSympathy} from "./actions/actionsSympathy";


const SYMPATHY = 'sympathy';

const initState = [
   /* {
        id: 1,
        title: 'Grand Prix Rose Hand tied',
        img: 'https://ld-prestashop.template-help.com/prestashop_62258/img/p/1/1/7/117-tm_home_default.jpg',
        params: {
            sort: "1450",
            articul : '844',
            size : 'Средний (25–35 см) ',
            type: 'Сборный букет'
        }
    }*/
]

const flowerSympathyReducer = (state = initState, action) => {
    switch(action.type){
        case 'ADD_SYMPATHY_FLOWERS':
            return[
                ...state, {...action.flower}
            ]
        case 'ADD_BADGE_SYMPATHY':
            return [...state.map(f => {
                if(f.id === action.badge.id) return {...f, badges: [...action.badge.names]}
                else return {...f}
            })]
        case 'DELETE_FLOWER_SYMPATHY':
            return [...state.filter(f => f.id !== action.flowerId)];
        default: return state;

    }
}



export const newFlowerForSympathy = (flower) => (dispatch) => {

    dispatch(addSympathyFlowers(flower));
}

export const addFlowersBadgesSympathy = (badge) => (dispatch) => {
    dispatch(addBadgeSympathy(badge))
}


export const deleteFlowerFromSympathy = (flowerId) => (dispatch) => {

    deleteFromStorage(flowerId, SYMPATHY);
    dispatch(deleteFlowerSympathy(flowerId));
}


export default flowerSympathyReducer;

