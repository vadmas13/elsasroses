import {addFlowersArray, addFlowersBadges, addFlowerState, clearFlowersState} from "../flowersReducer";
import {changeLoadingGetFlowers, getButtonsValues, getFlowersApi} from "../apiReducer";
import {cleanStateErr, errflowers, getBadges} from "../catalogReducer";

export const getFlowersCatalog = (params) => (dispatch, getState, {getFirebase, getFirestore}) => {


    dispatch(changeLoadingGetFlowers(true));
    dispatch(cleanStateErr());

    const db = getFirestore();
    const LIMIT = 10;
    let queryCategory = db.collection('flowersCategory');
    let queryBadges = db.collection('badges');
    let queryFlower = db.collection('flowers');
    queryFlower = params.min ? queryFlower.where('price', '>=', parseInt(params.min)) : queryFlower;
    queryFlower = params.max ? queryFlower.where('price', '<=', parseInt(params.max)) : queryFlower;
    queryFlower = params.colors && params.colors.length !== 0 ?
        queryFlower.where('colors', 'array-contains-any', [...params.colors.map(c => `#${c}`)]) : queryFlower;


    let getBadgesRequest = () => {
         queryBadges.get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                let flowerData = doc.data();
                dispatch(addFlowersBadges(flowerData));
                if(flowerData.id === 'default') dispatch(getBadges(flowerData));
            })
        })
    }


    queryFlower.get()
        .then(document => {
            if (document.docs.length !== 0) {
                dispatch(clearFlowersState());
                document.docs.map((doc, i) => {
                    let flowerData = {...doc.data()};
                    if (params.category) {
                        queryCategory.where(params.category, '==', true)
                            .where('id', '==', flowerData.id).get().then((category) => {
                            if (params.badges) {
                                queryBadges.where('id', '==', flowerData.id).where('names', 'array-contains', params.badges).get()
                                    .then((flowerBadges) => {
                                        let isThisFlower = flowerBadges.docs.length !== 0 ? !!flowerBadges.docs[0].data().id : false;
                                        if (isThisFlower) dispatch(addFlowerState(flowerData));
                                        if (document.docs.length === i + 1) {
                                            dispatch(getFlowersApi());
                                            getBadgesRequest()
                                        }
                                    })
                            } else {
                                let isThisFlower = category.docs.length !== 0 ? !!category.docs[0].data().id : false;
                                if (isThisFlower) dispatch(addFlowerState(flowerData));
                                if (document.docs.length === i + 1) {
                                    dispatch(getFlowersApi());
                                    getBadgesRequest()
                                }
                            }
                        })
                    } else {
                        dispatch(addFlowerState(flowerData));
                        if (document.docs.length === i + 1) {
                            dispatch(getFlowersApi());
                            getBadgesRequest();
                        }
                    }
                })
            } else {
                throw('Нет цветов по данному запросу');
            }
        }).catch((err) => {
        dispatch(changeLoadingGetFlowers(false));
        dispatch(getFlowersApi());
        dispatch(errflowers(err));
    })

}

export const FlowersCategories = () => (dispatch, getState, {getFirebase, getFirestore}) => {
    let db = getFirestore();

}
