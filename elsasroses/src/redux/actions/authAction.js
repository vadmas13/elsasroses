import {checkAuth} from "../authReducer";
import firebase from "../../config/fbConfig";
import {addUserIdToReview} from "../addReviewsReducer";

export const signIn = (credentials) =>
    (dispatch, getState, {getFirebase}) => {
        const firebase = getFirebase();

        firebase.auth().signInWithEmailAndPassword(
            credentials.email,
            credentials.password
        ).then(() => {
            dispatch({type: 'LOGIN_SUCCESS'})
        }).catch((err) => {
            dispatch({type: 'LOGIN_ERROR'}, err)
        })
    }

export const signOut = () =>
    (dispatch, getState, {getFirebase}) => {
        const firebase = getFirebase();

        firebase.auth().signOut().then(() => {
            dispatch({type: 'SIGNOUT_SUCCESS'})
        }).catch((err) => {
            dispatch({type: 'SIGNOUT_ERROR'}, err)
        })
    }

export const signUp = (newUser) =>
    (dispatch, getState, {getFirebase, getFirestore}) => {
        const firebase = getFirebase();
        const firestore = getFirestore();

        firebase.auth().createUserWithEmailAndPassword(
            newUser.email,
            newUser.password
        ).then((response) => {
            return (
                firestore.collection('users').doc(response.user.uid).set({
                    firstName: newUser.firstName,
                    lastName: newUser.lastName,
                    initials: newUser.firstName[0] + newUser.lastName[0],
                    email: newUser.email,
                    password: newUser.password
                })
            )
        }).then(() => {
            dispatch({type: "SIGNUP_SUCCESS"})
        }).catch((err) => {
            dispatch({type: "SIGNUP_ERROR", err})
        })
    }


export const signInWithGoogle = () => (dispatch, getState, {getFirebase, getFirestore}) => {
    const firebase = getFirebase();
    const db = getFirestore();
    let provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider).then(function(result) {
        // This gives you a Google Access Token. You can use it to access the Google API.
        let token = result.credential.accessToken;
        // The signed-in user info.
        let user = result.user;
        let userNames = user.displayName.split(' ');
        db.collection('users').doc(user.uid).set({
            displayName: user.displayName,
            initials: userNames[0][0] + userNames[1][0],
            email: user.email,
            password: 'GOOGLE'
        });
        dispatch({type: 'LOGIN_SUCCESS'})
        // ...
    }).catch(function(error) {
        let errorMessage = error.message;
        dispatch({type: 'LOGIN_ERROR'}, errorMessage)
    });
}

export const checkAuthUser = () => (dispatch, getState, {getFirebase, getFirestore}) => {
    const db = getFirestore();
    firebase.auth().onAuthStateChanged((user) => {
        if (user) {
            db.collection('users').doc(user.uid).get().then((doc) => {
                let userData = doc.data();
                let name = user.displayName ? user.displayName : `${userData.firstName} ${userData.lastName}`;
                let userState = {name, uid: user.uid, level: userData.level ? userData.level : false};
                dispatch(addUserIdToReview(user.uid, name));
                dispatch(checkAuth(true, userState));
            })
        } else {
            dispatch(checkAuth(false))
        }
    });
}