
export const getAllOrders = () => (dispatch, getState, {getFirebase, getFirestore}) => {
    let db = getFirestore();
    let orders = [];

    db.collection('orders').get()
        .then(document => {
            document.docs.map((doc, i) => {
                const order = {...doc.data(), id: doc.id};
                orders.push(order);
            })
        }).then(() => dispatch({type: 'GET_ALL_ORDERS', orders}))
}

export const getStatusOrders = (status) => (dispatch, getState, {getFirebase, getFirestore}) => {
    let db = getFirestore();
    let orders = [];

    db.collection('orders').where('status', '==', status).get()
        .then(document => {
            document.docs.map((doc, i) => {
                const order = {...doc.data(), id: doc.id};
                orders.push(order);
            })
        }).then(() => dispatch({type: 'GET_ALL_ORDERS', orders}))
}

export const changeOrderStatus = (trackNumber, status, order) => (dispatch, getState, {getFirebase, getFirestore}) => {
    let db = getFirestore();
    db.collection('orders').doc(trackNumber).set({
        ...order, status
    }).then(() =>  dispatch({type: 'CHANGE_STATUS_ORDER', trackNumber, status}))
        .catch(err => {console.log(err)})
    ;
}