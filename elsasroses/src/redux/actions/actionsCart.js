
const CREATE_NEW_STATE_FLOWER = 'CREATE_NEW_STATE_FLOWER';

export const setNewFlowerProps = (flowerId, price) => ({type: CREATE_NEW_STATE_FLOWER, flowerId, price})
