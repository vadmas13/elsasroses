import {deleteFromStorage} from "../../api/localStorage";

export const sendOrderFormData = (orderData, auth) => (dispatch, getState, {getFirebase, getFirestore}) => {

    const COUNT = 'count', PRICE = 'price';

    let getResultPriceCount = (typeResult, flowers = orderData.flowers) => {
        let result = 0;
        flowers.forEach(flower => {
            if(typeResult === COUNT){
                result += parseInt(flower.count)
            }else if(typeResult === PRICE){
                let price = flower.price.replace(' ', '');
                result += parseInt(price);
            }
        })
        return result
    }

    let db = getFirestore();
    let user, trackNumber = 79345 + Math.floor(Math.random() * 10000);
    let resultPrice = getResultPriceCount(PRICE), resultCount = getResultPriceCount(COUNT)
    if(auth.isAuth) user = auth.user.uid;
    else user = '';

    let createdAt = new Date();

    db.collection('orders').add({
        ...orderData, status: 'В обработке', trackNumber, user, createdAt , resultPrice, resultCount
    }).then(() => { debugger
        dispatch({type: 'SUCCESS_SEND_DATA', trackNumber});
        deleteFromStorage('all', 'card');
    }).catch((err) => {console.log(err)})
}