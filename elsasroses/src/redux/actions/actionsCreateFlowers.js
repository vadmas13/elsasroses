export const addNewFlowerInfo = (values) => (dispatch, getState, {getFirebase, getFirestore}) => {

    let db = getFirestore();
    let newArticle = values.lastArticle + 1;
    let title = values.title;

    db.collection('flowers').add({
        id: newArticle,
        title: values.title,
        price: values.price ? values.price : null,
        oldPrice: values.oldPrice ? values.oldPrice : null,
        colors: values.colors ? values.colors : [''],
        content: values.content,
        linkImg: values.linkImg ? values.linkImg : null,
        rate: 4
    }).then(() => {
        dispatch({type: 'ADD_NEW_PARAMS', values: {newArticle, title}});
        dispatch({type: 'ADD_FLOWER', values})
    }).catch((err) => {
        // dispatch({type: 'CREATE_PROJECT_ERROR', err})
    })


};


export const addNewFlowerParams = (values) => (dispatch, getState, {getFirebase, getFirestore}) => {

    let db = getFirestore();
    let newArticle = values.lastArticle;

    db.collection('params').add({
        id: newArticle,
        gipercium: values.params.gipercium,
        lenta: values.params.lenta,
        size: values.params.size,
        rose: values.params.rose,
        sizal: values.params.sizal,
        sortBy: values.params.sortBy,
        type: values.params.type,
        fistashka: values.params.fistashka,
        sharChristmas: values.params.sharChristmas

    }).then(() => {
        dispatch({type: 'ADD_FLOWER_PARAMS', values})
    }).catch((err) => {
        // dispatch({type: 'CREATE_PROJECT_ERROR', err})
    })


};

export const addFlowerFilters = (values) => (dispatch, getState, {getFirebase, getFirestore}) => {
    let db = getFirestore();
    let {lastArticle, checkedFilters} = values;

    let {levels, multiParams} = checkedFilters;

    let addTofirebase = (obj) => {
        if (obj) {
            Object.keys(obj).forEach(lvl => {
                let arrayTitles, currentLVLFilter = obj[lvl];
                if(currentLVLFilter && currentLVLFilter.titles) arrayTitles = JSON.stringify(currentLVLFilter.titles.split(',').map(t => t.trim()));
                else  arrayTitles = [];
                currentLVLFilter = {...currentLVLFilter, titles: arrayTitles};

                if (lvl === 'imagesFilters') currentLVLFilter = {images: JSON.stringify(currentLVLFilter.images)};
                else if (lvl === 'priceFilters') currentLVLFilter = {price: JSON.stringify(currentLVLFilter.price)};

                db.collection(lvl).add({
                    ...currentLVLFilter, id: lastArticle
                }).then(() => dispatch({type: 'ADD_NEW_FILTERS', currentLVLFilter}))
            })
        }else dispatch({type: 'ADD_NEW_FILTERS'})
    }

    addTofirebase(levels);
    addTofirebase(multiParams);

}

export const setFlowerBadges = (values) => (dispatch, getState, {getFirebase, getFirestore}) => {
    let db = getFirestore();

    let {lastArticle, badges} = values;

    let badgesTitles = Object.keys(badges).filter(b => badges[b]);

    db.collection('badges').add({
        names: [...badgesTitles], id: lastArticle
    }).then(() => dispatch({type: 'ADD_FLOWER_BADGES', badgesTitles}))
}


