const GET_CATEGORY_LIST = 'GET_CATEGORY_LIST';
const SET_CATEGORIES = 'SET_CATEGORIES';

export const getCategories = () => (dispatch, getState, {getFirebase, getFirestore}) => {
    let db = getFirestore();
    let categories = [...[]];

    db.collection('categoryList').get()
        .then(document => {

             document.docs.map((doc, i) => {
                const {id, name} = doc.data();
                const length = document.docs.length - 1;

                db.collection('childrenOfCategory').doc(id + '').get()
                    .then(children => {
                        let category = {id, name, categories: children.data()};  // object
                        categories.push(category);
                        if (length === i) dispatch({type: GET_CATEGORY_LIST, categories});
                    })

            })


        })

}

export const setCategories = (values) => (dispatch, getState, {getFirebase, getFirestore}) => {

    let db = getFirestore();
    let article = values.lastArticle;

    db.collection('flowersCategory').doc(article + '').set({
        id: article,
        ...values.categories
    }).then(() => {
        dispatch({type: SET_CATEGORIES, categories: {...values.categories}});
    }).then(() => {
        dispatch({type: 'ADD_FLOWER_CATEGORY'})
    })

}