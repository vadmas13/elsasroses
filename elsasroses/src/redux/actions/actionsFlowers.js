const CHANGE_BUTTON = 'CHANGE_BUTTON';
const CHECK_BUTTONS = 'CHECK_BUTTONS';
const ADD_FLOWER = 'ADD_FLOWER';
const ADD_FLOWERS_BADGES = 'ADD_FLOWERS_BADGES';
const DELETE_FLOWER_FROM_STATE = 'DELETE_FLOWER_FROM_STATE';
const CLEAR_FLOWERS_STATE = 'CLEAR_FLOWERS_STATE';
const ADD_FLOWERS_ARRAY = 'ADD_FLOWERS_ARRAY';

export const changeButton = ({flowerId, type}) => ( { type: CHANGE_BUTTON, data: {flowerId, type}});
export const checkButtons = (values) => ({ type: CHECK_BUTTONS, values});

export const addFlower = (flower) => ({type: ADD_FLOWER, flower});
export const flowerBadge = (badge) => ({type: ADD_FLOWERS_BADGES, badge});

export const deleteFlowerCart = (flowerId) => ({type: DELETE_FLOWER_FROM_STATE, flowerId});
export const clearFlowers = () => ({type:CLEAR_FLOWERS_STATE})

export const addFlowersArrayCatalog = (flowers) => ({type: ADD_FLOWERS_ARRAY, flowers})




