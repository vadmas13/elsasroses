const ADD_ID_COMPARE_FLOWER = 'ADD_ID_COMPARE_FLOWER';
const COPY_ID_COMPARE_FLOWERS_FROM_STORAGE = 'COPY_ID_COMPARE_FLOWERS_FROM_STORAGE';

const ADD_ID_SYMPATHY_FLOWER = 'ADD_ID_SYMPATHY_FLOWER';
const COPY_ID_SYMPATHY_FLOWERS_FROM_STORAGE = 'COPY_ID_SYMPATHY_FLOWERS_FROM_STORAGE';

const ADD_ID_CARD_FLOWER = 'ADD_ID_CARD_FLOWER';
const COPY_ID_CARD_FLOWERS_FROM_STORAGE = 'COPY_ID_CARD_FLOWERS_FROM_STORAGE';

const GET_CARD_COUNTS = 'GET_CARD_COUNTS';
const FOLD_CARD_COUNTS = 'FOLD_CARD_COUNTS';

export const idFlowerForCompare = (flowerId) => ({ type:ADD_ID_COMPARE_FLOWER, flowerId});
export const copyFlowersFromStorageToCompare = (flowers) => ({ type: COPY_ID_COMPARE_FLOWERS_FROM_STORAGE, flowers});

export const idFlowerForSympathy = (flowerId) => ({ type:ADD_ID_SYMPATHY_FLOWER, flowerId});
export const copyFlowersFromStorageToSympathy = (flowers) => ({ type: COPY_ID_SYMPATHY_FLOWERS_FROM_STORAGE, flowers});

export const idFlowerForCard = (flowerId) => ({ type:ADD_ID_CARD_FLOWER, flowerId});
export const copyFlowersFromStorageToCard = (flowers) => ({ type: COPY_ID_CARD_FLOWERS_FROM_STORAGE, flowers});

export const getCardCounts = (arrayCounts) => ({type: GET_CARD_COUNTS, arrayCounts});

export const foldCardCounts = () => ({type: FOLD_CARD_COUNTS})