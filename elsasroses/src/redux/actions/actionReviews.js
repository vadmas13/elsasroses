import * as moment from 'moment';
import 'moment/locale/ru';
import {addReviewProcess} from "../reviewsListReducer";
import {clearStateReview} from "../addReviewsReducer";

const ADD_REVIEW_TO_LIST = 'ADD_REVIEW_TO_LIST';

export const addReview = (data, id) => (dispatch, getState, {getFirebase, getFirestore}) => {

    dispatch(addReviewProcess());

    const db = getFirestore();
    let date  = new Date();
    let timestamp = moment().format();
    let review = {
        ...data, id, date
    };
    let reviewToState = {
        ...data, id, date: timestamp
    };
    db.collection('reviews').add({
        ...review
    }).then(() => {
        dispatch({type: ADD_REVIEW_TO_LIST, reviewToState})
        dispatch(clearStateReview())
    });
}