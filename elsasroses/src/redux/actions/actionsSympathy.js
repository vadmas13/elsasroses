const ADD_SYMPATHY_FLOWERS = 'ADD_SYMPATHY_FLOWERS';

const DELETE_FLOWER_SYMPATHY = 'DELETE_FLOWER_SYMPATHY';
const ADD_BADGE_SYMPATHY = 'ADD_BADGE_SYMPATHY';

export const addSympathyFlowers = (flower) => ({ type:ADD_SYMPATHY_FLOWERS, flower});

export const deleteFlowerSympathy = (flowerId) => ({ type: DELETE_FLOWER_SYMPATHY, flowerId});

export const addBadgeSympathy = (badge) => ({ type: ADD_BADGE_SYMPATHY, badge});