const GET_LIST_REVIEWS = 'GET_LIST_REVIEWS';

export const getList = (id) => (dispatch, getState, {getFirebase, getFirestore}) => {
    const db = getFirestore();
    db.collection('reviews').where('id', '==', id).get()
        .then(collection => {
            return collection.docs.map(d => {
                return d.data();
            })
        }).then(data => {
        dispatch({type: GET_LIST_REVIEWS, data})
    })
}