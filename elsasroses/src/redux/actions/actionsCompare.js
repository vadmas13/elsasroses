const ADD_COMPARE_FLOWERS = 'ADD_COMPARE_FLOWERS';
const ADD_FLOWER_PARAMS_FOR_COMPARE = 'ADD_FLOWER_PARAMS_FOR_COMPARE';
const DELETE_FLOWER = 'DELETE_FLOWER';
const ADD_BADGE_COMPARE = 'ADD_BADGE_COMPARE';


export const addCompareFlowers = (flower) => ({ type:ADD_COMPARE_FLOWERS, flower});
export const getParams = (params) => ({ type:ADD_FLOWER_PARAMS_FOR_COMPARE, params});
export const deleteFlower = (flowerId) => ({ type: DELETE_FLOWER, flowerId});
export const addBadgeCompare = (badge) => ({ type: ADD_BADGE_COMPARE, badge});