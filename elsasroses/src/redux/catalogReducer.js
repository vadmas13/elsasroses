const GET_COLORS_BASE = 'GET_COLORS_BASE';
const SET_ERROR_FLOWERS = 'SET_ERROR_FLOWERS';
const CLEAN_STATE_ERR = 'CLEAN_STATE_ERR';
const CHANGE_USE_COLORS = 'CHANGE_USE_COLORS';
const SET_DEFAULT_COLLAPSE = 'SET_DEFAULT_COLLAPSE';
const SET_INIT_COLLAPSE = 'SET_INIT_COLLAPSE';
const GET_BAGDES = 'GET_BAGDES';

const InitState = {
    err: false,
    params: [],
    filters: {
        colors: []
    },
    defaultActiveCollapse: '', // for categories
    badges: {}
}

const catalogReducer = (state = InitState, action) => {
    switch (action.type) {
        case 'SET_CATALOG_FLOWERS':
            return {...state, flowers: [...action.flowers], initState: {loading: false, loaded: true}};
        case GET_COLORS_BASE:
            return {
                ...state, filters: {
                    ...state.filters,
                    colors: [...action.colors.map(c => {
                        if(action.useColors) {
                            if(action.useColors.some(color => `#${color}` === c)){
                                return {name: c, use: true}
                            }else{
                                return {name: c, use: false}
                            }
                        }else return {name: c, use: false}
                    })]
                }
            };
        case SET_ERROR_FLOWERS:
            return {...state, err: action.err}
        case CLEAN_STATE_ERR:
            return {...state, err: false}
        case CHANGE_USE_COLORS:
            return {
                ...state, filters: {
                    ...state.filters, colors: [...state.filters.colors.map(c => {
                        if (c.name === action.name) {
                            return {name: c.name, use: !c.use}
                        } else return {...c}
                    })]
                }
            }
        case SET_DEFAULT_COLLAPSE:
            return {...state, defaultActiveCollapse: action.categories.filter(c =>
                {
                    return Object.keys(c.categories).some(category => action.param.trim() === category.trim())
                }
                )[0]['id']}
        case SET_INIT_COLLAPSE:
            return {...state, defaultActiveCollapse: InitState.defaultActiveCollapse}
        case GET_BAGDES:
            return {
                ...state, badges: { names: [...action.badges.names], colors: [...action.badges.colors] }
            }
        default:
            return state
    }
}

export const getColorsBase = (colors, useColors) => (dispatch) => {
    if(useColors && useColors.length !==0) dispatch({type: GET_COLORS_BASE, colors, useColors});
    else dispatch({type: GET_COLORS_BASE, colors});
}

export const setDefaultCollapse = (categories, param) => dispatch => {
    dispatch({type: SET_DEFAULT_COLLAPSE, categories, param})
}

export const setInitCollapse = () => dispatch => {
    dispatch({type: SET_INIT_COLLAPSE})
}

export const cleanStateErr = () => (dispatch) => {
    dispatch({type: CLEAN_STATE_ERR});
}

export const getBadges = (badges) => ({type: GET_BAGDES, badges})

export const changeUsedColors = (name) => (dispatch) => {
    dispatch({type: CHANGE_USE_COLORS, name})
}

export const errflowers = (err) => (dispatch) => {
    dispatch({type: SET_ERROR_FLOWERS, err})
}

export default catalogReducer;