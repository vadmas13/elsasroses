
const GET_ORDERS_PROFILE = 'GET_ORDERS_PROFILE';
const LOADING_ORDERS = 'LOADING_ORDERS';
const LOADED_ORDERS = 'LOADED_ORDERS';
const GET_REQUEST_ORDER = 'GET_REQUEST_ORDER';
const GET_ALL_ORDERS = 'GET_ALL_ORDERS';
const CHANGE_STATUS_ORDER = 'CHANGE_STATUS_ORDER';

let InitialState = {
   orders: [],
    requestOrder: '',
    loadOrders: {
       loaded: false, loading: false
    }
};

const profileReducer = (state = InitialState, action) => {
    switch(action.type) {
        case GET_ORDERS_PROFILE:
            return {...state, orders:  [...state.orders, {...action.orders}]}
        case LOADING_ORDERS:
            return {...state, loadOrders: {loaded: false, loading: true}}
        case LOADED_ORDERS:
            return {...state, loadOrders: {loaded: true, loading: false}}
        case GET_REQUEST_ORDER:
            return {...state, requestOrder: action.value }
        case GET_ALL_ORDERS:
            return {...state, orders: [...action.orders]}
        case CHANGE_STATUS_ORDER:
            return {...state,
                orders: [...state.orders.map(order => {
                    if(order.id === action.trackNumber) return {...order, status: action.status};
                    else return {...order}
                })]}
        default:
            return state;
    }
}

export const getOrders = (orders) => (dispatch) =>{
    dispatch({type : GET_ORDERS_PROFILE, orders})
}

export const loadingOrders = () => (dispatch) => {
    dispatch({ type: LOADING_ORDERS})
}


export const getRequestOrder = (value) => (dispatch) => {
    dispatch({type: GET_REQUEST_ORDER, value})
}

export const loadedOrders = () => (dispatch) => {
    dispatch({ type: LOADED_ORDERS})
}


export default profileReducer