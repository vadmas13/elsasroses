import {deleteFromStorage, newArrayWithStorage} from "../api/localStorage";
import {
    foldCardCounts,
    getCardCounts,
    idFlowerForCard,
    idFlowerForCompare,
    idFlowerForSympathy
} from "./actions/actionsNavbar";
import {setNewFlowerProps} from "./actions/actionsCart";

const COMPARE = 'compare', SYMPATHY = 'sympathy', CARD = 'card';
const CHECK_STATE_STORAGE = 'CHECK_STATE_STORAGE_';
const CHANGE_BUTTONS_CART = 'CHANGE_BUTTONS_CART';
const CHANGE_BUTTON_CARD_WHEN_CHANGE_COUNTS = 'CHANGE_BUTTON_CARD_WHEN_CHANGE_COUNTS';
const CREATE_NEW_STATE_FLOWER = 'CREATE_NEW_STATE_FLOWER';
const CHANGE_RESULT_PRICE = 'CHANGE_RESULT_PRICE';
const DELETE_FLOWER_FROM_CARD = 'DELETE_FLOWER_FROM_CARD';
const SET_CURRENT_PRICE = 'SET_CURRENT_PRICE';

const initState = [];

const cartReducer = (state = initState, action) => {
    switch (action.type) {
        case CREATE_NEW_STATE_FLOWER:
            return [...state, state.every(f => f.id !== action.flowerId) ?
                {
                    id: action.flowerId,
                    compare: false,
                    sympathy: false,
                    card: {
                        count: 0,
                        added: false,
                        price: 0
                    },
                    isChecked: false,
                    resultPrice: action.price,
                    status: 'created'
                } : null
            ];

        case 'CHECK_STATE_STORAGE_compare':
            return [
                ...state.map(f => {
                    if (f.id === action.flowerId) return {...f, compare: action.compare}
                    else return {...f}
                })
            ];
        case 'CHECK_STATE_STORAGE_sympathy':
            return [
                ...state.map(f => {
                    if (f.id === action.flowerId) return {...f, sympathy: action.sympathy}
                    else return {...f}
                })
            ];
        case 'CHECK_STATE_STORAGE_card':
            return [
                ...state.map(f => {
                    if (f.id === action.flowerId)
                        return {...f, card: {
                            count: parseInt(action.card[1]),
                                added: action.card[0],
                                filterTitle: action.card[2] ? action.card[2].split('|')[1] : ''
                            },
                            isChecked: true}
                    else return {...f}
                })
            ];
        case CHANGE_BUTTONS_CART:
            return [
                ...state.map(f => {
                    if (parseInt(f.id) === action.flowerId)
                        return {
                            ...f, [action.field]: action.field === 'card' ? {count: action.card, added: true}
                                : !f[action.field]
                        }
                    else return {...f}
                })
            ];
        case CHANGE_BUTTON_CARD_WHEN_CHANGE_COUNTS:
            return [
                ...state.map(f => {
                    if (parseInt(f.id) === action.flowerId)
                        return {...f, card: {...f.card, added: action.counts === f.card.count}}
                    else return {...f}
                })

            ];
        case CHANGE_RESULT_PRICE:
            return [
                ...state.map(f => {
                    if (parseInt(f.id) === action.flowerId)
                        return {...f, resultPrice: action.resultPrice}
                    else return {...f}
                })
            ]
        case DELETE_FLOWER_FROM_CARD:
            return [...state.map(f => {
                if(parseInt(f.id) !== action.flowerId) return {...f}
                else return {id: f.id, status: 'deleted'}
            })]
        case SET_CURRENT_PRICE:
            return [...state.map(f => {
                if(parseInt(f.id) !== action.flowerId && f.status !== 'deleted') return {...f, card: {...f.card, price: action.price}}
                else return {...f}
            })]
        default:
            return state;
    }
}

export const getStateFromStorage = (flowerId) => (dispatch) => {

    let result, checkStorageElements = (storage, flowerId, typeStorage, dispatch, lastElem) => {
        if (typeStorage === 'card' && result.length !== 0) {
            result = storage.filter(id => {
                let countsId = id.split('_');
                return parseInt(countsId[0]) === parseInt(flowerId); // ДОБАВЬ ФИЛЬТРЫ !
            });
            if (result.length !== 0) result = result[0].split('_');
            else result = [false, 1];
        } else {
            result = storage.filter(id => id === parseInt(flowerId));
            result = !!result.length;
        }
        dispatch({type: CHECK_STATE_STORAGE + [typeStorage], [typeStorage]: result, flowerId})
    }

    const flowersCompare = JSON.parse(window.localStorage.getItem(COMPARE));
    const flowersSympathy = JSON.parse(window.localStorage.getItem(SYMPATHY));
    const flowersCard = JSON.parse(window.localStorage.getItem(CARD));

    if (typeof flowerId === 'object') flowerId = flowerId.id;

    checkStorageElements(flowersCompare, flowerId, COMPARE, dispatch);
    checkStorageElements(flowersSympathy, flowerId, SYMPATHY, dispatch);
    checkStorageElements(flowersCard, flowerId, CARD, dispatch);

    return true;
}

export const changeButtonsCart = (flowerId, field, count, isCart, filterTitle) => (dispatch) => {
    let arrayCounts = newArrayWithStorage(flowerId, field, count, isCart, filterTitle);
    if (!arrayCounts) arrayCounts = [];
    dispatch({type: CHANGE_BUTTONS_CART, field, card: count, flowerId});
    switch (field) {
        case 'sympathy':
            return dispatch(idFlowerForSympathy(arrayCounts));
        case 'compare' :
            return dispatch(idFlowerForCompare(arrayCounts));
        case 'card' :
            return (
                dispatch(idFlowerForCard(arrayCounts)),
                    dispatch(getCardCounts(arrayCounts)),
                    dispatch(foldCardCounts())
            )
    }

}

export const changeButtonCardWhenChangeCounts = (counts, flowerId, resultPrice) => (dispatch) => {
    if(resultPrice){
        dispatch(changeButtonsCart(flowerId, CARD, counts, true));
    }
    dispatch(changeResultPrice(flowerId, resultPrice))
    dispatch({type: CHANGE_BUTTON_CARD_WHEN_CHANGE_COUNTS, counts, flowerId})
}

export const changeResultPrice = (flowerId, resultPrice)  => (dispatch) => {
    dispatch({type: CHANGE_RESULT_PRICE, resultPrice, flowerId})
}

export const createNewStateFlower = (flowerId, price) => (dispatch) => {
    dispatch(setNewFlowerProps(flowerId, price))
}

export const deleteFlowerFromCard = (flowerId) => (dispatch) => {
    deleteFromStorage(flowerId, CARD);
    dispatch({type: DELETE_FLOWER_FROM_CARD, flowerId})
}

export const setCurrentPrice = (flowerId, price) => (dispatch) => {
    dispatch({type: SET_CURRENT_PRICE, flowerId, price})
}

export default cartReducer;
