import {addBadgeCompare, addCompareFlowers, deleteFlower, getParams} from "./actions/actionsCompare";
import {deleteFromStorage} from "../api/localStorage";

const COMPARE = 'compare';

const initState = [
   /* {
        id: 1,
        title: 'Grand Prix Rose Hand tied',
        img: 'https://ld-prestashop.template-help.com/prestashop_62258/img/p/1/1/7/117-tm_home_default.jpg',
        params: {
            sort: "1450",
            articul : '844',
            size : 'Средний (25–35 см) ',
            type: 'Сборный букет'
        }
    }*/
]

const flowerCompareReducer = (state = initState, action) => {
    switch(action.type){
        case 'ADD_COMPARE_FLOWERS':
            return[
                ...state, {...action.flower}
            ]
        case 'ADD_FLOWER_PARAMS_FOR_COMPARE':
            return [ ...state.map(f => {
                if(f.id === action.params.id) return {...f, params: {...action.params}};
                else return {...f}
            })]
        case 'ADD_BADGE_COMPARE':
            return [...state.map(f => {
                if(f.id === action.badge.id) return {...f, badges: [...action.badge.names]}
                else return {...f}
            })]
        case 'DELETE_FLOWER':
            return [...state.filter(f => f.id !== action.flowerId)];
        default: return state;

    }
}



export const newFlowerForCompare = (flower) => (dispatch) => {

    dispatch(addCompareFlowers(flower));
}

export const addFlowersBadgesCompare = (badge) => (dispatch) =>{
    dispatch(addBadgeCompare(badge))
}

export const getParamsForFlower = (params) => (dispatch) => {
    dispatch(getParams(params));
}

export const deleteFlowerFromCompare = (flowerId) => (dispatch) => {

    deleteFromStorage(flowerId, COMPARE);
    dispatch(deleteFlower(flowerId));
}


export default flowerCompareReducer;

