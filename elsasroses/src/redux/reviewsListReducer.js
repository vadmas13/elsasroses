import {getList} from "./actions/actionReviewsList";

const GET_LIST_REVIEWS = 'GET_LIST_REVIEWS';
const CHECKING_REVIEWS_LIST = 'CHECKING_REVIEWS_LIST';
const ADD_REVIEW_TO_LIST = 'ADD_REVIEW_TO_LIST';
const ADD_REVIEW_PROCESS = 'ADD_REVIEW_PROCESS';


const initState = {
    isChecked: false,
    checking: false,
    comments: []
};

const reviewsListReducer = (state = initState, action) => {
    switch (action.type){
        case GET_LIST_REVIEWS:
            return {...state, comments: [...action.data], isChecked: true, checking: false}
        case CHECKING_REVIEWS_LIST:
            return {...state, checking: true, isChecked: false}
        case ADD_REVIEW_TO_LIST:
            return {...state, comments: [...state.comments, action.reviewToState], checking: false}
        case ADD_REVIEW_PROCESS:
            return {...state, checking: true}
        default: return state;
    }
}

export const getReviewsList = (id) => dispatch => {
    dispatch(getList(id))
}

export const addReviewProcess = () => dispatch => {
    dispatch({type: ADD_REVIEW_PROCESS})
}





export default reviewsListReducer;