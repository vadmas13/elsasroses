
const initState = {
    stage: 1,
    flowerInfo: {
        title:'',
        id: ''
    },
    flowerParams: {

    },
    flowerFilters: []
}

const flowerCreaterReducer = (state = initState, action) => {
    switch (action.type){
        case 'ADD_NEW_PARAMS':
            return {...state, stage: 2, flowerInfo: { title: action.values.title, id: action.values.newArticle}}
        case 'ADD_NEW_FILTERS':
            return {...state, stage: 3, flowerFilters: [...state.flowerFilters, action.currentLVLFilter] }
        case 'ADD_FLOWER_PARAMS':
            return {...state, stage: 4, flowerParams: {...action.values.params}}
        case 'ADD_FLOWER_CATEGORY':
            return {...state, stage: 5}
        case 'ADD_FLOWER_BADGES':
            return {...state, stage: 6, badges: {...action.badges}};
        default:
            return state;
    }
}

export default flowerCreaterReducer;