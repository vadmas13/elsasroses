import {getTrackNumber} from "../api/flowersAPI";

const CHANGE_TRACK_NUMBER = 'CHANGE_TRACK_NUMBER';
const GET_ORDER_DATA = 'GET_ORDER_DATA';
const GET_ORDER_DATA_EMPTY = 'GET_ORDER_DATA_EMPTY';

const initState = {
    trackNumber: ''
}

const trackOrderReducer = (state = initState, action) => {
    switch (action.type) {
        case CHANGE_TRACK_NUMBER:
            return {trackNumber: action.value};
        case GET_ORDER_DATA:
            return {...action.trackOrder}
        case GET_ORDER_DATA_EMPTY:
            return {...state, emptyText: "Данного трек номера нет в базе"}
            default:
                return state;
    }
}

export const getOrderData = (trackNumber) => (dispatch) => {
    dispatch(getTrackNumber(trackNumber));
}

export const changeTrackNumber = (value) => (dispatch) => {
    dispatch({type: CHANGE_TRACK_NUMBER, value })
}

export default trackOrderReducer;