import {getCategories, setCategories} from "./actions/actionsCategory";

const initState = [];


const categoryReducer = (state = initState, action) => {
    switch(action.type){
        case 'GET_CATEGORY_LIST':
            return [...action.categories];
        case 'SET_CATEGORIES':
            return [...state, { setCategories: {...action.categories}}];
        default:
            return state;
    }
}

export const getCategoriesList = () => (dispatch) =>{

     dispatch(getCategories())
}

export const setFlowerCategories = (values) => (dispatch) => {

    dispatch(setCategories(values))
}

export default categoryReducer;