import {
    copyFlowersFromStorageToCard,
    copyFlowersFromStorageToCompare,
    copyFlowersFromStorageToSympathy, foldCardCounts, getCardCounts, idFlowerForCard,
    idFlowerForCompare,
    idFlowerForSympathy
} from "./actions/actionsNavbar";
import {changeValueOfButton} from "./flowersReducer";
import {newArrayWithStorage} from "../api/localStorage";


const initState = {
    compareFlowersId: [],
    sympathyFlowersId: [],
    cardFlowersId: [],
    cardCounts: ''
}


const navbarReducer = (state = initState, action) => {
    switch (action.type) {
        case 'ADD_ID_COMPARE_FLOWER':
            return {...state, compareFlowersId: [...action.flowerId]};
        case 'COPY_ID_COMPARE_FLOWERS_FROM_STORAGE':
            return {...state, compareFlowersId: [...action.flowers]};
        case 'ADD_ID_SYMPATHY_FLOWER':
            return {...state, sympathyFlowersId: [...action.flowerId]};
        case 'COPY_ID_SYMPATHY_FLOWERS_FROM_STORAGE':
            return {...state, sympathyFlowersId: [...action.flowers]};
        case 'ADD_ID_CARD_FLOWER':
            return {...state, cardFlowersId: [...action.flowerId]};
        case 'COPY_ID_CARD_FLOWERS_FROM_STORAGE':
            return {...state, cardFlowersId: [...action.flowers]};
        case 'GET_CARD_COUNTS':
            return {...state, cardCounts: action.arrayCounts.map( id => {
                            let countId = id.split('_');
                            return countId[1];
                    })};
        case 'FOLD_CARD_COUNTS':
            return {...state, cardCounts: state.cardCounts.length ? state.cardCounts.reduce((summ, num) => {
                return parseInt(summ) + parseInt(num);
                }) : ''};
        default: {
            return state
        }
    }
}





export const newIdFlowerForCompare = (flowerId) => (dispatch) => {
    const type = 'compare';
    let arrayCounts = newArrayWithStorage(flowerId, type);
    if (!arrayCounts) arrayCounts = [];
    dispatch(changeValueOfButton({flowerId, type}));
    dispatch(idFlowerForCompare(arrayCounts));
}

export const newIdFlowerForSympathy = (flowerId) => (dispatch) => {
    const type = 'sympathy';
    let arrayCounts = newArrayWithStorage(flowerId, type);
    if (!arrayCounts) arrayCounts = [];
    dispatch(changeValueOfButton({flowerId, type}));
    dispatch(idFlowerForSympathy(arrayCounts));
}

export const newIdFlowerForCard = (flowerId) => (dispatch) => {
    const type = 'card';
    let arrayCounts = newArrayWithStorage(flowerId, type);
    if (!arrayCounts) arrayCounts = [];
    dispatch(changeValueOfButton({flowerId, type}));
    dispatch(idFlowerForCard(arrayCounts));
    dispatch(getCardCounts(arrayCounts));
    dispatch(foldCardCounts());
}

export const copyIdFlowersFromStorage = () => (dispatch) => {

    const flowersCompare = JSON.parse(window.localStorage.getItem('compare'));
    const flowersSympathy = JSON.parse(window.localStorage.getItem('sympathy'));
    const flowersCard = JSON.parse(window.localStorage.getItem('card'));


    dispatch(copyFlowersFromStorageToCompare(flowersCompare));
    dispatch(copyFlowersFromStorageToSympathy(flowersSympathy));
    dispatch(copyFlowersFromStorageToCard(flowersCard));
    dispatch(getCardCounts(flowersCard));
    dispatch(foldCardCounts());
}



export default navbarReducer;