import {sendOrderFormData} from "./actions/actionsOrderCard";

const RENDER_ORDER_FORM = 'RENDER_ORDER_FORM';
const CHANGE_FORM_ORDER = 'CHANGE_FORM_ORDER';
const SENDING_ORDER_DATA = 'SENDING_ORDER_DATA';
const ADD_FLOWER_IN_ORDER = 'ADD_FLOWER_IN_ORDER';

let InitialState = {
    orderCard: false,
    email: '',
    phone: '',
    address: '',
    name: '',
    text: '',
    comment: '',
    status: {
        loading: false,
        success: false
    },
    flowers: [],
    payMethod: 'Наличными при получении'
};

const orderCardReducer = (state = InitialState, action) => {
    switch (action.type) {
        case RENDER_ORDER_FORM:
            return {
                ...state, orderCard: true
            }
        case CHANGE_FORM_ORDER:
            return {...state, [action.filed]: action.value}
        case SENDING_ORDER_DATA:
            return {...state, status: {loading: true, success: false}};
        case 'SUCCESS_SEND_DATA':
            return {...state, status: {loading: false, success: true}, trackNumber: action.trackNumber};
        case ADD_FLOWER_IN_ORDER:
            return {...state, flowers: state.flowers.length !== 0 ? state.flowers.some((f, i) => {
                   if(f.flowerId === action.dataFlower.flowerId) {
                       state.flowers[i].count = action.dataFlower.count;
                       state.flowers[i].title = action.dataFlower.title;
                       state.flowers[i].price = action.dataFlower.price;
                       state.flowers[i].useFilters = action.dataFlower.useFilters;
                       return true;
                   }
                   else { return false; }
                }) ? [...state.flowers.map(f => {
                       if(f.flowerId === action.dataFlower.flowerId) return {...action.dataFlower}
                       else return {...f}
                    })] : [...state.flowers, {...action.dataFlower}] :
                    [...state.flowers,{...action.dataFlower}]

            }
        default:
            return state;
    }
}

export const addFlowerInOrder = (flowerId, count, title, filterTitle, price, useFilters) => (dispatch) => {
    dispatch({type: ADD_FLOWER_IN_ORDER, dataFlower: {flowerId, count, title, filterTitle, price, useFilters}})
}

export const renderOrderForm = () => (dispatch) => {
    dispatch({type: RENDER_ORDER_FORM})
}

export const onChangeFormOrder = (filed, value) => (dispatch) => {
    dispatch({type: CHANGE_FORM_ORDER, filed, value})
}

export const sendOrderData = (orderData, auth) => (dispatch) =>{
    dispatch({type: SENDING_ORDER_DATA});
    dispatch(sendOrderFormData(orderData, auth));
}

export default orderCardReducer