const CHANGE_REVIEW_DATA = 'CHANGE_REVIEW_DATA';
const ADD_UID_TO_REVIEW = 'ADD_UID_TO_REVIEW';
const CLEAR_STATE_REVIEW = 'CLEAR_STATE_REVIEW';


const initState = {
    id: '',
    comment: '',
    rating: 3.5,
    userId: '',
    userName: '',
    email: ''
}

const addReviewsReducer = (state = initState, action) => {
    switch (action.type){
        case CHANGE_REVIEW_DATA:
            return {...state, [action.field]: action.data}
        case ADD_UID_TO_REVIEW:
            return {...state, userId: action.uid, userName: action.name}
        case CLEAR_STATE_REVIEW: debugger;
            return {...state, email: initState.email, comment: initState.comment, rating: initState.rating}
        default: return state;
    }
}

export const addUserIdToReview = (uid, name) => dispatch => {
    dispatch({ type: ADD_UID_TO_REVIEW, uid, name})
}

export const changeRviewsData = (field, data) => dispatch => {
    dispatch({type: CHANGE_REVIEW_DATA, data, field})
}

export const clearStateReview = () => dispatch => {
    dispatch({type: CLEAR_STATE_REVIEW})
}




export default addReviewsReducer;