import {copyIdFlowersFromStorage} from "./navbarReducer";
import {getCategoriesList} from "./categoryReducer";
import {checkAuthUser} from "./actions/authAction";



const INITIALIZED_APP = "INITIALIZED-APP";


let InitialState = {
    Initialized: false,
    orderCard: false
};

const appReducer = (state = InitialState, action) => {
    switch (action.type) {
        case INITIALIZED_APP:
            return {
                ...state, Initialized: true
            }
        default:
            return state;
    }
}


export const initilizedApp = () => ({type: INITIALIZED_APP});

export const checkStorage = (typeStorage) => {
    const defaultStorage = () => localStorage.setItem(typeStorage, JSON.stringify([]));

    let checkTypeofStorage = () => localStorage.getItem(typeStorage);

    if (typeof checkTypeofStorage() !== 'string' || !checkTypeofStorage()) {
        return defaultStorage()
    } else {
        let storage = JSON.parse(checkTypeofStorage());
        if (storage) {
            return storage.length !== 0 && typeof storage !== 'string' && typeof storage !== 'number' ? storage :
                defaultStorage()
        } else {
            return defaultStorage();
        }
    }

}

export const  initilizedSuccess = () =>  (dispatch) => {

    let promiseCategoryList = ()  =>  dispatch(getCategoriesList()) ;

    Promise.all([
        promiseCategoryList(),
        dispatch(checkAuthUser())
    ]).then(() => {
        dispatch(initilizedApp());
        dispatch(copyIdFlowersFromStorage());
    })
}


export default appReducer;