import firebase from "../config/fbConfig";

const initState = {
    authError: null,
    isAuth: false,
    isLoadedAuth: false,
    user: {}
}

const authReducer = (state = initState, action) => {
    switch(action.type){
        case 'LOGIN_ERROR':
            console.log("error login");
            return {...state, authError: 'SignIn failed', isAuth: true, isLoadedAuth: true};
        case 'LOGIN_SUCCESS':
            console.log("success login");
            return {...state, authError: null}
        case 'SIGNOUT_SUCCESS':
            console.log("success SIGNOUT");
            return state;
        case 'SIGNUP_SUCCESS':
            console.log("success SIGNUP");
            return {
                ...state,
                authError: null,
                isLoadedAuth: true,
                isAuth: true
            } ;
        case 'SIGNUP_ERROR':
            console.log("ERROR SIGNUP");
            return {
                ...state,
                authError: action.err.message
            } ;
        case 'CHECK_AUTH':
            return {...state, isAuth: action.isAuth, isLoadedAuth: true, user: action.user? {...action.user} : {...state.user}}
        default:
            return state;

    }

}


export const checkAuth = (isAuth, user) => (dispatch) => {
    dispatch({type: 'CHECK_AUTH', isAuth, user})
}

export default authReducer;