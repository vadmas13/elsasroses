import React from 'react';
import {compose} from "redux";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import Preloader from "../components/common/Preloader/Preloader";
import {newIdFlowerForCard, newIdFlowerForCompare, newIdFlowerForSympathy} from "../redux/navbarReducer";
import {checkStorage} from "../redux/appReducer";
import {getFlowersColors, getLastFlowersAPI} from "../api/flowersAPI";
import {checkValuesOfButtons} from "../redux/flowersReducer";
import queryString from 'query-string'
import {changeLoadingGetFlowers, setInitialState} from "../redux/apiReducer";
import {changeUsedColors, setDefaultCollapse, setInitCollapse} from "../redux/catalogReducer";
import {getFlowersCatalog} from "../redux/actions/actionCatalog";


const mapStateToProps = (state, ownProps) => {
    return ({
        params: ownProps.location.search ? queryString.parse(ownProps.location.search) : false,
        flowers: state.flowers,
        api: state.api,
        catalog: state.catalog,
        categories: state.categories
    })
}

let checkParamsChanges = (nextParams, params) => {
    return Object.keys(nextParams).some(p => nextParams[p] !== params[p])
}

const getFlowersForCatalog = (Component) => {
    class Comp extends React.Component {

        state = {
            getParams: false
        }

        shouldComponentUpdate(nextProps, nextState){
            /*if((this.props.params && !this.state.getParams) ||
                checkParamsChanges(nextProps.params, this.props.params)) {  debugger
                this.setState({getParams: true})
                return true;
            }else if(!this.props.params){  debugger
                return true
            }else if(!this.props.params){

            }*/
            return true
        }

        render() {
            const {api, params, catalog, flowers, categories} = this.props;
            const {getFlowers, loadingFlowers, getButtonsValues} = api;
            if(typeof params.colors === 'string'){
                params.colors = [params.colors];
            }

            if (!getFlowers && !loadingFlowers) {
                if (params) {
                    this.props.getFlowersCatalog(params);
                } else this.props.getLastFlowersAPI();
            }

            if(params.category && categories.length !== 0 && !catalog.defaultActiveCollapse) {
                 this.props.setDefaultCollapse(categories, params.category);
            }

            if (catalog.filters.colors.length === 0) {
                this.props.getFlowersColors(params.colors);
            }


            if (!getButtonsValues && getFlowers && catalog.filters.colors.length !== 0) {
                let promiseStorageCompare = checkStorage('compare');
                let promiseStorageSympathy = checkStorage('sympathy');
                let promiseStorageCard = checkStorage('card');
                this.props.checkValuesOfButtons(
                    [
                        {name: 'compare', flowersId: promiseStorageCompare},
                        {name: 'sympathy', flowersId: promiseStorageSympathy},
                        {name: 'card', flowersId: promiseStorageCard}
                    ]
                )

            }

            if (getButtonsValues && getFlowers) {
                return <Component
                    catalogFilters={catalog.filters}
                    setInitialState={this.props.setInitialState}
                    err={catalog.err}
                    badges={catalog.badges}
                    defaultActiveCollapse={catalog.defaultActiveCollapse}
                    categories={categories}
                    params={params}
                    setInitCollapse={this.props.setInitCollapse}
                    changeUsedColors={this.props.changeUsedColors}
                    newIdFlowerForCompare={this.props.newIdFlowerForCompare}
                    newIdFlowerForSympathy={this.props.newIdFlowerForSympathy}
                    newIdFlowerForCard={this.props.newIdFlowerForCard}
                    flowers={flowers}/>
            } else return <Preloader/>;
        }
    }

    return compose(
        withRouter,
        connect(mapStateToProps, {
            getFlowersCatalog, newIdFlowerForCompare, checkStorage, changeLoadingGetFlowers,
            setInitialState, changeUsedColors, setDefaultCollapse, setInitCollapse,
            getLastFlowersAPI, checkValuesOfButtons, getFlowersColors,
            newIdFlowerForSympathy, newIdFlowerForCard
        })
    )(Comp);
}

export default getFlowersForCatalog;