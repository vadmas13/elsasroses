import React from 'react';
import {getFlowersIdCompareOrSympathy, getLastFlowersAPI} from "../api/flowersAPI";
import {newIdFlowerForCard, newIdFlowerForCompare, newIdFlowerForSympathy} from "../redux/navbarReducer";
import {compose} from "redux";
import {connect} from "react-redux";
import {checkValuesOfButtons, deleteFlowerFromState} from "../redux/flowersReducer";
import {checkStorage} from "../redux/appReducer";
import Preloader from "../components/common/Preloader/Preloader";

const mapStateToProps = (state) => {
    return ({
        flowers: state.flowers,
        api: state.api
    })
}


export const getFlowersForList = (typeStorage, Compon) => {
    class Comp extends React.Component {
        render() {
            const {getFlowers, getButtonsValues, loadingFlowers} = this.props.api;
            if (!getFlowers && !loadingFlowers) {
                if(!typeStorage) this.props.getLastFlowersAPI();
                else if(typeStorage === 'sympathy') this.props.getFlowersIdCompareOrSympathy(typeStorage);
            }
            if(!getButtonsValues && getFlowers) {
                let promiseStorageCompare = checkStorage('compare');
                let promiseStorageSympathy = checkStorage('sympathy');
                let promiseStorageCard = checkStorage('card');
                this.props.checkValuesOfButtons(
                    [
                        {name: 'compare', flowersId: promiseStorageCompare},
                        {name: 'sympathy', flowersId: promiseStorageSympathy},
                        {name: 'card', flowersId: promiseStorageCard}
                    ]
                )

            }
            let flowers = this.props.flowers;
            if (flowers && getButtonsValues && getFlowers) return (
                <Compon newIdFlowerForCompare={this.props.newIdFlowerForCompare}
                        flowers={flowers}
                        deleteFlowerFromState={this.props.deleteFlowerFromState}
                        newIdFlowerForSympathy={this.props.newIdFlowerForSympathy}
                        newIdFlowerForCard={this.props.newIdFlowerForCard}
                        getFlowersIdCompareOrSympathy={this.props.getFlowersIdCompareOrSympathy}
                />)
            else return <Preloader />
        }
    }

    return compose(
        connect(mapStateToProps, {
            newIdFlowerForCompare,
            newIdFlowerForSympathy,
            getFlowersIdCompareOrSympathy,
            newIdFlowerForCard,
            deleteFlowerFromState,
            checkValuesOfButtons,
            getLastFlowersAPI
        }),
    )(Comp);
}






