import React from 'react';
import {compose} from "redux";
import {firestoreConnect} from "react-redux-firebase";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import Preloader from "../components/common/Preloader/Preloader";
import {
    changeButtonCardWhenChangeCounts,
    changeButtonsCart, changeResultPrice,
    createNewStateFlower, deleteFlowerFromCard,
    getStateFromStorage, setCurrentPrice
} from "../redux/cartReducer";
import EmptyCard from "../components/Card/EmptyCard/EmptyCard";
import ResultCard from "../components/Card/ResultCard/ResultCard";
import OrderCard from "../components/Card/OrderCard/OrderCard";
import {addFlowerInOrder, onChangeFormOrder, renderOrderForm, sendOrderData} from "../redux/orderCardReducer";
import SuccessSendData from "../components/Card/SuccessSendData/SuccessSendData";
import {changeRviewsData} from "../redux/addReviewsReducer";
import {addReview} from "../redux/actions/actionReviews";
import {getReviewsList} from "../redux/reviewsListReducer";


const mapStateToProps = (state, ownProps) => {
    return {
        id: ownProps.match.params.id ? ownProps.match.params.id : ownProps.flowerId,
        setIsCard: ownProps.setIsCard,
        isCard: !!ownProps.match.params.id,
        cartStorage: state.cart,
        newReview: state.newReview,
        orderCard: state.orderCard,
        reviewsList: state.reviewsList,
        auth: state.auth,
        flower: {
            flowerInfo: state.firestore.ordered.flowers,
            flowerParams: state.firestore.ordered.params,
            flowerCategory: state.firestore.ordered.flowersCategory,
            firstlvlFilters: state.firestore.ordered.firstlvlFilters,
            secondlvlFilters: state.firestore.ordered.secondlvlFilters,
            thirdlvlFilters: state.firestore.ordered.thirdlvlFilters,
            imagesFilters: state.firestore.ordered.imagesFilters,
            priceFilters: state.firestore.ordered.priceFilters,
            badges: state.firestore.ordered.badges  // NEED TO TEST
        }
    }
}

const mathResultPrice = (flowersCard) => {
    if (flowersCard.length !== 0) return flowersCard.reduce((summ, f) => {
        if (f && f.resultPrice) {
            return summ + parseInt(f.resultPrice);
        }
        else return summ + 0
    }, 0);
    else return null
}

const mathResultCount = (flowersCard) => {
    if (flowersCard.length !== 0) return flowersCard.reduce((summ, f) => {
        if (f && f.card) return summ + parseInt(f.card.count)
        else return summ + 0
    }, 0);
    else return null
}

const textWithCount = (number) => {
    let titles = ['товар', 'товара', 'товаров'];
    let cases = [2, 0, 1, 1, 1, 2];
    return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}

const separate = (flowerFilter, field) => {
    let fieldName = 'titles';
    if (field) fieldName = field;
    if (flowerFilter && flowerFilter.id) return { //fix
        ...flowerFilter,
        [fieldName]: JSON.parse(flowerFilter[fieldName])
    };
    else return false;
}


const getDataWithId = (flowerId, data) => {
    let filterData = data.filter(d => d.id === parseInt(flowerId.id || flowerId));
    return {...filterData[0]}
}

const separateDataFilters = (Component) => {
    class Comp extends React.Component {

        render() {

            let {flower, cartStorage, id, isCard, orderCard, auth} = this.props;
            const {flowerInfo, flowerParams, flowerCategory, badges} = flower;

            let isReadyData = Object.keys(flower).every(prop => prop !== undefined && prop !== null);

            if (isReadyData) {
                if (orderCard.status.success) return <SuccessSendData isAuth={auth.isAuth}
                                                                      trackNumber={orderCard.trackNumber}/>
                else if (orderCard.status.loading) return <Preloader/>
                else if (Object.keys(flower).every(f => flower[f])) {
                    let paramsDefault;
                    if (typeof id !== 'object') {
                        id = [parseInt(id)];
                    }

                    let params = flowerParams.filter(p => {
                        if (p.id !== 'Артикул') return true;
                        else paramsDefault = p;
                    })

                    let cardsArray = [];

                    return id.map((flowerId, i) => {

                        let cartStorageParams = cartStorage.filter(s => s && s.id === (flowerId.id || flowerId));
                        let info = getDataWithId(flowerId, flowerInfo);
                        let resultPrice = parseInt(flowerId.count || cartStorageParams[0]) * info.price;
                        if (cartStorage.every(s => s && s.id !== (flowerId.id || flowerId))) {
                            this.props.createNewStateFlower(flowerId.id || flowerId, resultPrice);
                        }
                        if (cartStorageParams[0] && cartStorageParams[0].status !== 'deleted') {


                            if (cartStorageParams[0] && !cartStorageParams[0].isChecked) {
                                this.props.getStateFromStorage(flowerId)
                            } // FIX TO SEVERAL FLOWERS


                            let firstlvlFilters = separate(getDataWithId(flowerId, flower.firstlvlFilters));
                            let secondlvlFilters = separate(getDataWithId(flowerId, flower.secondlvlFilters));
                            let thirdlvlFilters = separate(getDataWithId(flowerId, flower.thirdlvlFilters));
                            let imagesFilters = separate(getDataWithId(flowerId, flower.imagesFilters), 'images');
                            let priceFilters = separate(getDataWithId(flowerId, flower.priceFilters), 'price');


                            let flowerData = {
                                flower: {
                                    flowerInfo: info,
                                    flowerParams: getDataWithId(flowerId, params),
                                    paramsDefault,
                                    flowerCategory: getDataWithId(flowerId, flowerCategory),
                                    badges: getDataWithId(flowerId, badges)
                                }, firstlvlFilters,
                                secondlvlFilters, thirdlvlFilters, imagesFilters, priceFilters,
                                cartStorage: cartStorageParams[0]
                            };


                            cardsArray.push(<Component {...flowerData} isCard={isCard} orderCard={orderCard}
                                                       addFlowerInOrder={this.props.addFlowerInOrder}
                                                       newReview={this.props.newReview}
                                                       reviewsList={this.props.reviewsList}
                                                       getReviewsList={this.props.getReviewsList}
                                                       addReview={this.props.addReview}
                                                       length={id.length} iterator={i} auth={auth}
                                                       sendOrderData={this.props.sendOrderData}
                                                       changeRviewsData={this.props.changeRviewsData}
                                                       deleteFlowerFromCard={this.props.deleteFlowerFromCard}
                                                       setCurrentPrice={this.props.setCurrentPrice}
                                                       changeResultPrice={this.props.changeResultPrice}
                                                       changeButtonCardWhenChangeCounts={this.props.changeButtonCardWhenChangeCounts}
                                                       changeButtonsCart={this.props.changeButtonsCart}/>);

                        } //else debugger; return <EmptyCard/>

                        if (id.length === i + 1) {
                            let resultPrice = mathResultPrice(cartStorage);
                            let resultCount = mathResultCount(cartStorage);
                            let countText = textWithCount(resultCount);
                            if (cardsArray.length !== 0) return (
                                <>
                                    {resultPrice && resultCount && flowerId.id ?
                                        orderCard.orderCard ?
                                            <OrderCard onChangeFormOrder={this.props.onChangeFormOrder}
                                                       auth={auth}
                                                       sendOrderData={this.props.sendOrderData}
                                                       orderCard={orderCard}/>
                                            :
                                            <ResultCard resultCount={resultCount}
                                                        setIsCard={this.props.setIsCard}
                                                        renderOrderForm={this.props.renderOrderForm}
                                                        countText={countText}
                                                        resultPrice={resultPrice}/> : null}
                                    {cardsArray}
                                </>
                            );
                            else return <EmptyCard/>
                        }

                    })

                } else {
                    return <Preloader/>
                }
            }
        }
    }

    return compose(
        withRouter,
        connect(mapStateToProps, {
            getStateFromStorage, changeButtonsCart,
            createNewStateFlower,
            renderOrderForm,
            addReview,
            getReviewsList,
            addFlowerInOrder,
            changeRviewsData,
            onChangeFormOrder,
            sendOrderData,
            setCurrentPrice,
            changeResultPrice,
            deleteFlowerFromCard,
            changeButtonCardWhenChangeCounts
        }),
        firestoreConnect((props) => {
            let {id} = props;
            id = typeof id === 'object' ? id : [{id: parseInt(id)}];

            let flowersId = id.map(id => parseInt(id.id));

            return [
                {collection: "flowers", where: ['id', 'in', [...flowersId]]},
                {collection: "params", where: ['id', 'in', [...flowersId, 'Артикул']]},
                {collection: "flowersCategory", where: ['id', 'in', [...flowersId]]},
                {collection: "firstlvlFilters", where: ['id', 'in', [...flowersId]]},
                {collection: "secondlvlFilters", where: ['id', 'in', [...flowersId]]},
                {collection: "thirdlvlFilters", where: ['id', 'in', [...flowersId]]},
                {collection: "imagesFilters", where: ['id', 'in', [...flowersId]]},
                {collection: "priceFilters", where: ['id', 'in', [...flowersId]]},
                {collection: "badges", where: ['id', 'in', [...flowersId]]},
            ]


        }),
    )(Comp);
}
export default separateDataFilters;