import React from 'react';
import {compose} from "redux";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";
import Preloader from "../components/common/Preloader/Preloader";
import Profile from "../components/Profile/Profile";
import {getRequestOrder, loadingOrders} from "../redux/profileReducer";
import {getOrdersToUser} from "../api/flowersAPI";
import {firestoreConnect} from "react-redux-firebase";

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        profile: state.profile,
        statusOrder: state.firestore.ordered.statusOrder
    }
}


export const getProfileData = (Compon) => {
    class Comp extends React.Component {
        render() {
            const {auth, profile, statusOrder} = this.props;

            if (auth.isAuth && !auth.authError) {
                if(!profile.loadOrders.loaded && !profile.loadOrders.loading) {
                    this.props.loadingOrders();
                    this.props.getOrdersToUser(auth.user.uid);
                }
                return (
                    <Compon user={auth.user} statusOrder={statusOrder}
                            profile={profile} getRequestOrder={this.props.getRequestOrder}/>
                )
            }else if(auth.isLoadedAuth) return <Redirect to='/'/>;
            else return <Preloader/>
        }
    }

    return compose(
        connect(mapStateToProps, {loadingOrders, getOrdersToUser, getRequestOrder}),
        firestoreConnect(() => {
            return[
                { collection: 'statusOrder' }
            ]
        })
    )(Comp);
}






