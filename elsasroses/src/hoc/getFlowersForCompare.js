import React from 'react';
import {compose} from "redux";
import {connect} from "react-redux";
import Preloader from "../components/common/Preloader/Preloader";
import {getFlowersIdCompareOrSympathy, getFlowersParamsForCompare} from "../api/flowersAPI";
import {firestoreConnect} from "react-redux-firebase";
import {deleteFlowerFromCompare} from "../redux/flowerCompareReducer";

const COMPARE = 'compare';

const mapStateToProps = (state) => {
    return ({
        flowers: state.compare,
        paramsDefault: state.firestore.ordered.params,
        api: state.api
    })
}



export const getFlowersForCompare = (Compon) => {
    class Comp extends React.Component {
        render() {
            const {getFlowers, loadingFlowers, loadingParamsForCompare, getParamsForCompare} = this.props.api;
            let flowers;

            if (!getFlowers && !loadingFlowers)  this.props.getFlowersIdCompareOrSympathy(COMPARE);
            if (!getParamsForCompare && !loadingParamsForCompare)  this.props.getFlowersParamsForCompare();

            flowers = this.props.flowers;

            if (flowers && getFlowers && getParamsForCompare) return (
                <Compon flowers={flowers} paramsDefault={this.props.paramsDefault} deleteFlowerFromCompare={this.props.deleteFlowerFromCompare}/>)
            else return <Preloader />
        }
    }

    return compose(
        connect(mapStateToProps, {getFlowersIdCompareOrSympathy, getFlowersParamsForCompare, deleteFlowerFromCompare}),
        firestoreConnect( [
                {collection: "params", doc: 'default'},
            ]
        ),
    )(Comp);
}




