import React from 'react';
import {compose} from "redux";
import {connect} from "react-redux";
import Preloader from "../components/common/Preloader/Preloader";

const mapStateToProps = (state) => {
    return ({
        flowers: state.flowers,
    })
}


export const getSameProduct = (Compon) => {
    class Comp extends React.Component {
        render() {
                return <Compon />
        }
    }

    return compose(
        connect(mapStateToProps)
    )(Comp);
}






