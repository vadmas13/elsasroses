import React from 'react';
import {compose} from "redux";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";
import Preloader from "../components/common/Preloader/Preloader";
import {getRequestOrder, loadingOrders} from "../redux/profileReducer";
import {changeOrderStatus, getAllOrders, getStatusOrders} from "../redux/actions/actionsGetAdminOrders";
import {ordersSortBy} from "../secondaryFunctions/sort/sortOrders";
import {firestoreConnect} from "react-redux-firebase";

const DATE = 'date';
const STATUS = 'status';

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        profile: state.profile,
        statusOrder: state.firestore.ordered.statusOrder
    }
}


export const getOrdersForControl = (Compon) => {
    class Comp extends React.Component {
        render() {
            const {auth, profile, statusOrder} = this.props;
            let orders = profile.orders;

            if (auth.isAuth && !auth.authError && auth.user.level && auth.user.level === 'admin') {
                if (!profile.loadOrders.loaded && !profile.loadOrders.loading) {
                    this.props.loadingOrders();
                    this.props.getAllOrders();
                }
                if(profile.orders.length !== 0) orders = ordersSortBy(DATE, profile.orders);
                return (
                    <Compon user={auth.user} orders={orders}
                            statusOrder={statusOrder ? statusOrder : []}
                            getAllOrders={this.props.getAllOrders}
                            getStatusOrders={this.props.getStatusOrders}
                            requestOrder={profile.requestOrder}
                            changeOrderStatus={this.props.changeOrderStatus}
                            getRequestOrder={this.props.getRequestOrder}/>
                )
            } else if (auth.isLoadedAuth) return <Redirect to='/'/>;
            else return <Preloader/>
        }
    }

    return compose(
        connect(mapStateToProps, {loadingOrders, getAllOrders, getRequestOrder,
            changeOrderStatus, getStatusOrders}),
        firestoreConnect(() => {
            return [
                {collection :'statusOrder' }
            ]
        })
    )(Comp);
}






