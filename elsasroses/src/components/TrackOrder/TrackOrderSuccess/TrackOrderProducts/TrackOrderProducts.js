import React from 'react'
import styles from './../../TrackOrder.module.scss'


const TrackOrderProducts = (props) => {
    const {trackOrder, fromTrackOrderContainer} = props;

    return (
            <div className={`${styles.TrackOrderSuccess__item} ${!trackOrder.focus && !fromTrackOrderContainer ? styles.TrackOrderSuccess__hidden : ''}`}>
                <h6 className={styles.titleOrderCard}><i className="small material-icons">local_florist</i> Товары в
                    заказе</h6>
                {trackOrder.flowers.map(f => {
                    return (
                        <div className={styles.TrackOrderSuccess__flowerInfo}>
                            <div className={styles.TrackOrderSuccess__params}>
                                <p className={styles.TrackOrderSuccess__paramsTitle}>Артикул</p>
                                <p className={styles.TrackOrderSuccess__paramsValue}>{f.flowerId}</p>
                            </div>
                            <div className={styles.TrackOrderSuccess__params}>
                                <p className={styles.TrackOrderSuccess__paramsTitle}>Название</p>
                                <a href={`/cart/${f.flowerId}`} className={styles.TrackOrderSuccess__titleflower}>
                                    <p className={styles.TrackOrderSuccess__paramsValue}>{f.title}</p>
                                </a>
                            </div>
                            <div className={styles.TrackOrderSuccess__params}>
                                <p className={styles.TrackOrderSuccess__paramsTitle}>Количество</p>
                                <p className={styles.TrackOrderSuccess__paramsValue}>{f.count}</p>
                            </div>
                            <div className={styles.TrackOrderSuccess__params}>
                                <p className={styles.TrackOrderSuccess__paramsTitle}>Сумма</p>
                                <p className={styles.TrackOrderSuccess__paramsValue}> {props.resultPrice(f.count, f.price)} ₽</p>
                            </div>
                        </div>
                    )
                })}
            </div>
    )

}

export default TrackOrderProducts