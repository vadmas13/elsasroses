import React from 'react'
import styles from './../TrackOrder.module.scss'
import TrackOrderProducts from "./TrackOrderProducts/TrackOrderProducts";
import TrackOrderInfo from "./TrackOrderInfo/TrackOrderInfo";


const TrackOrderSuccess = (props) => {
    const {trackOrder, setFocusOrder, fromTrackOrderContainer, statusOrder, order, resultPrice} = props;
    const COLOR = 'color', ICON = 'icon';

    let changeFocusOrder = (e) => {
        if (!fromTrackOrderContainer) {
            let id = e.currentTarget.id;
            setFocusOrder(parseInt(id));
        }
    }

    let getStatusColorIcon = (statusOrder, status, typeParam) => {
        let statusParams = statusOrder.filter(so => so.status === status);
        if (typeParam === COLOR) return statusParams[0].color;
        if (typeParam === ICON) return statusParams[0].icon;
    }

    return (
        <div className={styles.TrackOrderSuccess}>
            <TrackOrderInfo trackOrder={trackOrder}
                            getStatusColorIcon={getStatusColorIcon}
                            changeFocusOrder={changeFocusOrder}
                            fromTrackOrderContainer={fromTrackOrderContainer}
                            statusOrder={statusOrder}
                            order={order}
                            setFocusOrder={setFocusOrder}/>
          <TrackOrderProducts trackOrder={trackOrder}
                              resultPrice={resultPrice}
                              fromTrackOrderContainer={fromTrackOrderContainer}/>
        </div>
    )

}

export default TrackOrderSuccess