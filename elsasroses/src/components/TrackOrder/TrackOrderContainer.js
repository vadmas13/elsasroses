import React from 'react'
import {compose} from "redux";
import {connect} from 'react-redux'
import TrackOrder from './TrackOrder'
import {changeTrackNumber, getOrderData} from "../../redux/trackOrderReducer";
import {firestoreConnect} from "react-redux-firebase";

const TrackOrderContainer = (props) => {
    const {trackOrder, getOrderData, changeTrackNumber} = props;
    const fromTrackOrderContainer = true;

    let resultPrice = (count, price) => {
        return parseInt(count) * parseInt(price.replace(' ', ''));
    }

    return <TrackOrder resultPrice={resultPrice} getOrderData={getOrderData}
                       statusOrder={props.statusOrder}
                       fromTrackOrderContainer={fromTrackOrderContainer}
                       trackOrder={trackOrder} changeTrackNumber={changeTrackNumber}/>
}

const mapStateToProps = (state) => {
    return{
        trackOrder: state.trackOrder,
        statusOrder: state.firestore.ordered.statusOrder
    }
}

export default compose(
    connect(mapStateToProps, {getOrderData, changeTrackNumber}),
    firestoreConnect(() => {
        return[
            { collection: 'statusOrder' }
        ]
    })
)(TrackOrderContainer);