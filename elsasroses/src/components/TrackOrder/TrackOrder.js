import React from 'react'
import styles from './TrackOrder.module.scss'
import TrackOrderSuccess from "./TrackOrderSuccess/TrackOrderSuccess";


const TrackOrder = (props) => {
    let {trackOrder} = props;

    let handleSubmit = (e) => {
        e.preventDefault();
        props.getOrderData(parseInt(trackOrder.trackNumber));
    }

    let onChangeValue = (e) => {
        let value = e.target.value;
        props.changeTrackNumber(value);
    }

    return (
        <div className={styles.TrackOrder}>
            <form>
                <div className={`row ${styles.TrackOrder__row}`}>
                    <div className="input-field col s12">
                        <i className="material-icons prefix">search</i>
                        <input id="icon_prefix" type="text" className="validate" onChange={onChangeValue}
                               value={trackOrder.trackNumber}/>
                        <label htmlFor="icon_prefix">Введите трек номер</label>
                        <button className={styles.button} onClick={handleSubmit}>Поиск заказа</button>
                        { trackOrder.emptyText ?
                            <div className={styles.TrackOrderEmpty}>
                                { trackOrder.emptyText }
                            </div>
                           : trackOrder.flowers ?
                                <TrackOrderSuccess fromTrackOrderContainer={props.fromTrackOrderContainer}
                                                   statusOrder={props.statusOrder}
                                    resultPrice={props.resultPrice} trackOrder={trackOrder} order={trackOrder} />
                            : null
                        }
                    </div>
                </div>
            </form>
        </div>
    )

}

export default TrackOrder