import React from 'react';
import styles from './Sympathy.module.css'
import Flower from "../FlowersList/Flower/Flower";

const Sympathy = (props) =>{
    const {flowers, deleteFlowerFromState} = props;
    const typeState = 'sympathy';

    return(
        <div className={styles.container}>
        { flowers && flowers.length ? <Flower flowers={flowers}
                                              typeState={typeState}
                                              deleteFlowerFromState={deleteFlowerFromState}
                                              newIdFlowerForCompare={props.newIdFlowerForCompare}
                                              newIdFlowerForCard={props.newIdFlowerForCard}
        /> : <p>Нет цветов</p>}
        </div>
    )
}

export default Sympathy