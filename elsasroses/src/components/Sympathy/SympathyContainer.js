import React from 'react'
import Sympathy from "./Sympathy";
import {getFlowersForList} from "../../hoc/getFlowersForList";


const SympathyContainer = (props) => {
    const {flowers, deleteFlowerFromSympathy, deleteFlowerFromState, newIdFlowerForCompare, newIdFlowerForCard} = props;

    return(
        <Sympathy flowers={flowers} deleteFlowerFromSympathy={deleteFlowerFromSympathy}
                  newIdFlowerForCompare={newIdFlowerForCompare}  newIdFlowerForCard={newIdFlowerForCard}
                  deleteFlowerFromState={deleteFlowerFromState} />
    )
}

export default getFlowersForList('sympathy' ,SympathyContainer)