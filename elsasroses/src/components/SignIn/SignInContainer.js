import React, {useState, useEffect, Component} from 'react'
import SignIn from "./SignIn";
import {signIn, signInWithGoogle} from "../../redux/actions/authAction";
import {connect} from 'react-redux'
import {Redirect} from "react-router-dom";

class SignInContainer extends Component {

    state = {
        email: '',
        password: '',
        firstName: '',
        lastName: ''
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.signIn(this.state)
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    render() {
        const { authError, authorize, signInWithGoogle} = this.props;
        if(authorize.uid) return <Redirect to='/' />
        return (
            <SignIn handleSubmit={this.handleSubmit} signInWithGoogle={signInWithGoogle}
                    authError={authError} handleChange={this.handleChange}/>
        )
    }
}

const mapStateToProps = (state) => {
    return({
        authError: state.auth.authError,
        authorize: state.firebase.auth
    })
}

export default connect(mapStateToProps, {signIn, signInWithGoogle})(SignInContainer)