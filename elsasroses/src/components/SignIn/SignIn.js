import React from 'react'
import styles from './SignIn.module.scss'
import googleImg from './../../assets/images/google.png'

const SignIn = (props) =>{
    const {authError, signInWithGoogle} = props;

    return(
        <div className={styles.login}>
            <form onSubmit={props.handleSubmit} className="white">
                <h5 className="grey-text text-darken-3">Войти</h5>
                <div className="input-field">
                    <label htmlFor="email">Email</label>
                    <input type="email" id="email" onChange={props.handleChange}/>
                </div>
                <div className="input-field">
                    <label htmlFor="password">Пароль</label>
                    <input type="password" id="password" onChange={props.handleChange}/>
                </div>
                <div className="input-field">
                    <button className="btn pink lighten-1 z-depth-0">Войти</button>
                </div>
                <div className={`input-field ${styles.iconsContainer}`}>
                    <a onClick={signInWithGoogle} className={styles.icons}>
                        <img src={googleImg} alt="Войти через гугл"/>
                    </a>
                    <div id="vk_auth">kjk</div>
                </div>

                <div className="red-text center">
                    { authError ?  <p>{authError}</p> : null }
                </div>
            </form>
        </div>
    )
}

export default SignIn;