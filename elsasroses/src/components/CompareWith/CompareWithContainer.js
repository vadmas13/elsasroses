import React from 'react';
import CompareWith from "./CompareWith";
import './../../App.scss';
import {compose} from "redux";
import {getFlowersForCompare} from "../../hoc/getFlowersForCompare";


class CompareWithContainer extends React.Component {
    render() {
        const {flowers, paramsDefault, deleteFlowerFromCompare} = this.props;
        return (
            <div className={'wrapperFlowers'}>
                <div className={'containerFlowers'}>
                    { flowers.length ?  <CompareWith flowersCompare = {flowers}
                                                     deleteFlowerFromCompare={deleteFlowerFromCompare}
                                                     paramsDefault={paramsDefault}/> : <p>Нечего сравнивать</p>}
                </div>
            </div>
        )
    }
}



export default compose(
    getFlowersForCompare
)(CompareWithContainer);

