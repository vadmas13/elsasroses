import React from 'react';
import style from './CompareWith.module.scss'
import StarsRate from "../FlowersList/Flower/StarsRate/StarsRate";


const CompareWith = (props) => {
    const {flowersCompare, paramsDefault} = props;
    let deleteFlower = (e) =>{
        e.preventDefault();
        let id = e.currentTarget.id;
        props.deleteFlowerFromCompare(parseInt(id));
    }

    return (
        <div className={style.scrollTable}>
            <table className={style.tableCompare}>
                <tr>
                    <td></td>
                    {flowersCompare.map(f => {
                        return <td className={style.tableCompare__cardTd}>
                            <a onClick={deleteFlower} className={style.close} id={f.id}><i className="material-icons">close</i></a>
                            <div className={style.cardFlower}>
                                <div className={style.cardImage}>
                                    <img src={f.linkImg} alt={'img'}/>
                                </div>
                                <div className={style.cardContent}>
                                    <span className={style.cardTitle}>{f.title}</span>
                                    <div className={style.price}>
                                        <p>{f.price} ₽</p>
                                    </div>
                                    <div className={style.price}>
                                        <StarsRate rate={f.rate}/>
                                    </div>
                                </div>
                            </div>
                        </td>
                    })}
                </tr>
                {Object.keys(paramsDefault[0]).map(key => {
                    if (flowersCompare.some(f => f.params[key])) {
                        return (
                            <tr>
                                <th className={style.tableCompare__sortBy}>{ paramsDefault[0][key] }</th>
                                {flowersCompare.map(f => {
                                    if (f.params[key]) {
                                        return <td> { f.params[key] }</td>
                                    }else return <td></td>
                                })}
                            </tr>
                        )
                    } else return null
                })}

            </table>
        </div>
    )
}

export default CompareWith;

