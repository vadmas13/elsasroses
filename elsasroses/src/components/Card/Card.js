import React from 'react';
import styles from './Card.module.scss'
import Gallery from "./gallegy/Gallery";
import Filters from "./filters/Filters";
import Price from "./price/Price";
import Preloader from "../common/Preloader/Preloader";
import CartHeader from "./cartHeader/CartHeader";
import DeleteFromCard from "./DeleteFormCard/DeleteFromCard";


const Card = (props) => {
    const {counter, filterTitle, setFilterTitle, images, price, setTitles, filters, resultPrice, deleteFlowerFromCard,
        newIdFlowerForSympathy, newIdFlowerForCompare, cartStorage, changeButtonsCart, changeButtonCardWhenChangeCounts} = props;
    const {flowerInfo, badges} = props.flower;


    if(flowerInfo.length !== 0){
        return(
                <div className={'containerFlowers'}>
                    <div className={styles.cardContainer}>
                        <DeleteFromCard id={flowerInfo.id} deleteFlowerFromCard={deleteFlowerFromCard}/>
                        <div className={styles.cardContainer__pictures}>
                            <Gallery badges={badges} images={images} />
                        </div>
                        <div className={styles.cardContainer__info + ' ' + styles.info}>

                            <CartHeader id={flowerInfo.id} title={flowerInfo.title} price={price}
                                        counter={counter}
                                        changeButtonCardWhenChangeCounts={changeButtonCardWhenChangeCounts}
                                        resultPrice={resultPrice}
                            />

                            <Price id={flowerInfo.id} newIdFlowerForSympathy={newIdFlowerForSympathy}
                                   newIdFlowerForCompare={newIdFlowerForCompare}
                                   compare={cartStorage ? cartStorage.compare : null}
                                   changeButtonsCart={changeButtonsCart}
                                   sympathy={cartStorage ? cartStorage.sympathy : null } />

                            <Filters setTitles={setTitles}
                                     filterTitle={filterTitle} filters={filters} setFilterTitle={setFilterTitle}/>

                        </div>
                    </div>
                </div>
        )}else return <Preloader />
}

export default Card;