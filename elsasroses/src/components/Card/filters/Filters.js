import React from 'react';
import styles from './../Card.module.scss'

const Filters = (props) => {
    const {filters, setFilterTitle, filterTitle, setTitles} = props;

    let setFilter = (e) => {
        let id = e.currentTarget.id;
        let title = id.split('_');
        setFilterTitle(setTitles(filterTitle ,title[0], title[1]));
    }

    let checkActiveTitles = (filterTitle, title) =>{
        if(filterTitle.indexOf(title) !== -1) return true;
        else return false
    }

    return (
        <div>
            {filters.map((filter, i) => {
                    return (
                        filter ?
                            <div className={styles.filters}>
                                <p>{filter.name}</p>
                                <div className={styles.filters__item}>
                                    {filter.titles.map((title, n) => {
                                        let isActive = checkActiveTitles(filterTitle, title) ? styles.filters__chipActive : '';
                                        return (

                                            <div className={`chip ${styles.filters__chip} ${isActive}`} id={`${title}_${i}`} onClick={setFilter}>
                                                {title}
                                            </div>

                                        )
                                    })

                                    }
                                </div>
                            </div>
                            : null
                    )
                }
            )
            }
        </div>
    )
}

export default Filters;



