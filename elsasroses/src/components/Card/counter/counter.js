import React from 'react';
import styles from './../Card.module.scss'

const Counter = (props) => {

    const {counter, id, changeButtonCardWhenChangeCounts, price} = props;
    const priceInt = typeof price === 'string' ? parseInt(price.replace(' ', '')) : price;

    let counterPlus = () => {
        let count = counter.count + 1;
        let resultPrice = priceInt * count;
        if(counter.count > 20) counter.setCount(20);
        else {
            counter.setCount(count);
            changeButtonCardWhenChangeCounts(count, id, resultPrice);
        }

    }
    let counterMinus = () => {
        let count = counter.count - 1;
        let resultPrice = priceInt * count;
        if(count < 1) counter.setCount(1);
        else {
            counter.setCount(count);
            changeButtonCardWhenChangeCounts(count, id, resultPrice);
        }

    }


    return (
        <div className={styles.info__submit + ' ' + styles.submit}>
            <div className={styles.counter}>
                <a className={styles.item} onClick={counterMinus}>
                    <i className="material-icons">remove</i>
                </a>
                <div className={styles.item + ' ' + styles.item__number}>
                    {counter && counter.count}
                </div>
                <a className={styles.item} onClick={counterPlus}>
                    <i className="material-icons">add</i>
                </a>
            </div>
        </div>

    )
}

export default Counter;