import React from 'react';
import styles from './../Card.module.scss'

const Price = (props) => {

    const {sympathy, compare, changeButtonsCart, id} = props;

    let compareFLower = (e) => {
        let id = parseInt(e.currentTarget.id);
        changeButtonsCart(id, 'compare')
    }
    let sympathyFlower = (e) => {
        let id = parseInt(e.currentTarget.id);
        changeButtonsCart(id, 'sympathy');
    }

    return (
        <div className={styles.cartHeader}>
            <div className={styles.iconsBox}>
                <div className={styles.iconsPanel}>
                    <a onClick={sympathyFlower} id={id}
                       className={sympathy ? styles.active : ''}>
                        <i className="small material-icons">favorite</i>
                    </a>
                </div>
                <div className={styles.iconsPanel}>
                    <a onClick={compareFLower} id={id}
                       className={compare ? styles.active : ''}>
                        <i className="small material-icons">shuffle</i>
                    </a>
                </div>
            </div>
        </div>
    )
}

export default Price;

