import React from 'react';
import styles from '../Card.module.scss'


const SuccessSendData = (props) => {
    const {trackNumber, isAuth} = props;

    return (
        <div className={styles.successOrderData}>
          <h5>Заказ успешно отправлен в обработку</h5>
            <p>Ваш трековый номер для отслеживания статуса заказа (запомните его): <span>{trackNumber}</span></p>
            {isAuth ? <a className={styles.button} href={'/profile'}>Мои заказы</a> : null}
            <p>Получить информацию о заказе можно <a href="/trackorder">здесь</a></p>
        </div>
    )
}

export default SuccessSendData;