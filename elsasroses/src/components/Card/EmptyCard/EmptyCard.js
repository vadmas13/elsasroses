import React from 'react';

const EmptyCard = () => {
    return (
        <>
            <div>
                Корзина пуста
            </div>
            <a>
                Перейти в каталог
            </a>
        </>
    )
}

export default EmptyCard