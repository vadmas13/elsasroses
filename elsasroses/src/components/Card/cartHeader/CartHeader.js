import React from 'react';
import styles from './../Card.module.scss'
import Counter from "../counter/counter";

const CartHeader = (props) => {
    const {price, title, id, changeButtonCardWhenChangeCounts, counter, resultPrice} = props;

    return (
        <div className={styles.cartHeader}>
            <div className={styles.titleHeader}>
                <h1 className={styles.title}>{title}</h1>
                Артикул: {id}
            </div>
            <div className={styles.info__price}>
                { price } ₽
            </div>
            <div>
                <Counter counter={counter} price={price}
                         changeButtonCardWhenChangeCounts={changeButtonCardWhenChangeCounts}
                         id={id} />
            </div>
            <div className={styles.info__price}>
                { resultPrice } ₽
            </div>
        </div>
    )
}

export default CartHeader;