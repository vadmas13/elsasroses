import React from 'react';
import styles from './../Card.module.scss'

const Charastericstics = (props) => {
    const {flowerParams, paramsDefault} = props;

    return (
        <div className={styles.info__countFlowers}>
            <p className={styles.info__title}>Характеристики</p>
            {Object.keys(flowerParams).map(p => {
                if (flowerParams[p] && p !== 'id') {
                    return (
                        <p className={styles.info__params}>
                            {paramsDefault[p]} <span> — </span> {flowerParams[p]}
                        </p>
                    )
                } else return null;
            })}
        </div>
    )
}

export default Charastericstics;

