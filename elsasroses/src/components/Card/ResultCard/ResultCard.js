import React from 'react';
import styles from './../Card.module.scss'

const ResultCard = (props) => {

    const {resultPrice, resultCount, countText, renderOrderForm, setIsCard} = props;

    let goInOrderCard = () => {
        setIsCard();
        renderOrderForm()
    }

    return (

        <div className={styles.resultCard}>
            <div className={styles.resultCard__counts}>
                В корзине <span>{resultCount}</span> {countText}
            </div>
            <div className={styles.resultCard__price}>
                Итого:  <span>{resultPrice} ₽ </span>
            </div>
            <button className={styles.button} onClick={goInOrderCard}>
                <i className="large material-icons">check</i> Оформить заказ
            </button>
        </div>
    )
}

export default ResultCard;

