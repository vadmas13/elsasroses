import React from 'react';
import styles from './../Card.module.scss'

const DeleteFormCard = (props) => {
    const {id, deleteFlowerFromCard} = props;

    let deleteFlower = (e) => {
        e.preventDefault();
        let fId = parseInt(e.currentTarget.id);
        deleteFlowerFromCard(fId);
    }

    return(
        <div className={styles.closeBlock}>
            <a onClick={deleteFlower} className={styles.close} id={id}><i className="material-icons">close</i></a>
        </div>
    )
}

export default DeleteFormCard