import React from 'react';
import styles from './../../Card.module.scss'

const Filters = (props) => {
    const {filters, filterTitle, getUseFilters, useFilters} = props;

    let checkActiveTitles = (filterTitle, title) =>{
        if(filterTitle.indexOf(title) !== -1) return true;
        else return false
    }

    let setUseFilters = (checkedFilters) => {
        return getUseFilters(checkedFilters);
    }

    const existFilters = filters.filter(f => f);

    let checkedFilters = [];

    return (
        <div>
            {existFilters.map((filter, i) => {
                    return (
                        filter ?
                            <div className={`${styles.filters} ${styles.filtersInline}`}>
                                <p>{filter.name } :</p>
                                <div className={`${styles.filters__item} ${styles.filtersInline__item}`}>
                                    {filter.titles.map((title, n) => {
                                        let isActive = checkActiveTitles(filterTitle, title) ? styles.filters__chipActive : false;
                                        if(isActive){
                                            checkedFilters.push({name: filter.name, title});
                                            if(existFilters.length === i + 1 && !useFilters){ setUseFilters(checkedFilters)}
                                            return (
                                            <div className={`chip ${styles.filters__chip} ${isActive}`}>
                                                {title}
                                            </div>

                                        )} else return null
                                    })

                                    }
                                </div>
                            </div>
                            : null
                    )

                }
            )
            }
        </div>
    )
}

export default Filters;



