import React from 'react';
import styles from './../../Card.module.scss'

const Header = (props) => {
    const {price, title, id, counter, resultPrice} = props;

    return (
        <div className={styles.cartHeader}>
            <div className={`${styles.titleHeader} ${styles.titleHeader__order}`}>
                <h1 className={styles.title}>{title}</h1>
                Артикул: {id}
            </div>
            <div>
                <p className={`${styles.info__text} ${styles.info__textTitle}`}>Цена</p>
                <p className={`${styles.info__text} ${styles.info__textPrice}`}>{ price } ₽</p>
            </div>
            <div>
                <p className={`${styles.info__text} ${styles.info__textTitle}`}>Количество</p>
                <p className={styles.info__text}>{counter && counter.count} шт.</p>
            </div>
            <div>
                <p className={`${styles.info__text} ${styles.info__textTitle}`}>Сумма</p>
                <p className={`${styles.info__text} ${styles.info__textPrice}`}>{ resultPrice } ₽ </p>
            </div>
        </div>
    )
}

export default Header;