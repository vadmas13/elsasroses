import React from 'react';
import styles from '../Card.module.scss'
import Gallery from "../gallegy/Gallery";
import Preloader from "../../common/Preloader/Preloader";
import Header from "./Header/Header";
import Filters from "./Filters/Filters";

const OrderInfo = (props) => {
    const {
        counter, filterTitle, images, price, resultPrice, filters, length, useFilters,
        iterator, orderCard, auth, getUseFilters
    } = props;
    const {flowerInfo} = props.flower;

    return (
        <div className={styles.orderedCard}>
            {flowerInfo.length !== 0 ?
                (
                    <div className={'containerFlowers'}>
                        {iterator === 0 ?
                            <h6 className={`${styles.titleOrderCard} ${styles.titleOrderCard__margin}`}>
                            <i className="small material-icons">local_florist</i> Товары в
                                корзине</h6>
                            : null
                        }
                        <div className={`${styles.cardContainer} ${styles.cardContainerOrder}`}>
                            <div className={styles.cardContainer__onePicture}>
                                <Gallery badges={null} images={[images[0]]}/>
                            </div>
                            <div className={styles.cardContainer__info + ' ' + styles.info}>

                                <Header id={flowerInfo.id} title={flowerInfo.title} price={price}
                                        counter={counter}
                                        resultPrice={resultPrice}/>
                                <Filters useFilters={useFilters} getUseFilters={getUseFilters} filterTitle={filterTitle}
                                         filters={filters}/>

                            </div>
                        </div>
                    </div>
                )
                : <Preloader/>}
        </div>
    )
}

export default OrderInfo;