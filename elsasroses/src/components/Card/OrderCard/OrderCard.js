import React from 'react';
import styles from './../Card.module.scss'


const OrderCard = (props) => {

    const {orderCard, auth} = props;

    let handleChange = (e) => {
        let field = e.target.id;
        if(e.target.type === 'radio') {
            let radioFiled = field.split('_');
            field = radioFiled[0]
        }
        let value = e.target.value;
        props.onChangeFormOrder(field, value);
    }
    let handleSubmit = () => {
        props.sendOrderData(orderCard, auth);
    }


    return (
        <>
            <div className={styles.containerOrder}>
                <h1 className={'title'}>Оформление заказа</h1>
                <form className="white" onSubmit={handleSubmit}>
                    <div className={styles.orderedCard}>
                        <div className={styles.formOrder}>
                            <div className={styles.formOrder__service}>
                                <h6 className={styles.titleOrderCard}><i
                                    className={"small material-icons "}>local_shipping</i> Способ доставки</h6>
                                <div className={styles.formOrder__title}>
                                    <label>
                                        <input className="with-gap" name="group1" onChange={props.handleChange}
                                               type="radio" checked/>
                                        <span>Доставка курьером</span>
                                    </label>
                                </div>
                                <div className={styles.formOrder__price}>
                                    <div>290 ₽</div>
                                    <div>до 2 часов</div>
                                </div>
                            </div>
                            <div className={styles.formOrder__service}>
                                <h6 className={styles.titleOrderCard}><i
                                    className={"small material-icons "}>account_balance_wallet</i> Способ оплаты</h6>
                                <div className={styles.formOrder__title}>
                                    <label>
                                        <input className="with-gap" name="payMethod" onChange={handleChange}
                                               checked="checked"
                                               type="radio" id={'payMethod_cash'}
                                               value={'Наличными при получении'} />
                                        <span>Наличными при получении</span>
                                    </label>
                                </div>
                                <div className={styles.formOrder__title}>
                                    <label>
                                        <input className="with-gap" name="payMethod" onChange={handleChange}
                                               type="radio" id={'payMethod_Sber'}  value={'Прямой перевод на Сбербанк'}/>
                                        <span>Прямой перевод на Сбербанк MasterCard: 5336 6900 4312 9404, Натали Викторовна Ж.</span>
                                    </label>
                                </div>
                                <div className={styles.formOrder__title}>
                                    <label>
                                        <input className="with-gap" name="payMethod" onChange={handleChange}
                                               type="radio" id={'payMethod_card'} value={'Яндекс.Касса'}/>
                                        <span>Яндекс.Касса | Банковские карты [Весь мир] Apple Pay, Google Pay и Сбербанк Онлайн</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div className={styles.formOrder}>
                            <h6 className={styles.titleOrderCard}>
                                <i className="small material-icons">account_box</i> Покупатель</h6>


                            <div className="input-field">
                                <label htmlFor="email">E-mail</label>
                                <input type="text" id="email" className={'validate'} onChange={handleChange}
                                       value={orderCard.email} required/>
                            </div>
                            <div className="input-field">
                                <label htmlFor="phone">Телефон</label>
                                <input type="text" id="phone" className={'validate'} onChange={handleChange}
                                       value={orderCard.phone} required/>
                            </div>
                            <div className="input-field">
                                <label htmlFor="address">Адрес доставки</label>
                                <textarea id="address" onChange={handleChange} value={orderCard.address}
                                          className='materialize-textarea' required></textarea>
                            </div>
                            <div className="input-field">
                                <label htmlFor="name">Имя получателя</label>
                                <input type="text" id="name" className={'validate'} value={orderCard.name}
                                       onChange={handleChange} required/>
                            </div>
                            <div className="input-field">
                                <label htmlFor="text">Текст открытки</label>
                                <textarea id="text" onChange={handleChange} value={orderCard.text}
                                          className='materialize-textarea' ></textarea>
                            </div>
                            <div className="input-field">
                                <label htmlFor="comment">Комментарий к заказу</label>
                                <textarea id="comment" onChange={handleChange} value={orderCard.comment}
                                          className='materialize-textarea' ></textarea>
                            </div>
                            <div className="input-field">
                                <button className={styles.button} onSubmit={handleSubmit}>Оформить заказ</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </>
    )
}

export default OrderCard