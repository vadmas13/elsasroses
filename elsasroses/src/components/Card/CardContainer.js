import React, {useState} from 'react';
import {compose} from "redux";

import styles from './Card.module.scss'
import {getCardStorage} from "../../api/localStorage";
import CartContainer from "../Cart/CartContainer";
import EmptyCard from "./EmptyCard/EmptyCard";



const CardContainer = (props) => {
    const {flowersCard} = props;


    let [flowersId, setFlowersId] = useState(getCardStorage());
    let [isCard, setIsCard] = useState(true);

    return (
        <div className={styles.containerCard}>
                <div className={styles.containerCard__flowerItem}>
                    { isCard ? <h1 className={'title'}>Корзина</h1> : null }
                    { flowersId.length !== 0 ?

                    <CartContainer flowerId={flowersId} setIsCard={setIsCard} />
                        : <EmptyCard /> }
                </div>
        </div>
    )

}


export default compose(

)(CardContainer);




