import React, {Component} from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Preloader from "../../common/Preloader/Preloader";
import styles from './../Card.module.scss'
import './GallerySlick.css'

export default class Gallery extends Component {

    render() {

        let {images, badges} = this.props;


        const settings = {
            customPaging: function (i) {
                return (
                    <a className={'customImage'}>
                        <img src={`${images[i]}`}/>
                    </a>
                );
            },
            dots: true,
            dotsClass: "slick-dots slick-thumb",
            infinite: true,
            speed: 500,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1
        };
        return (
            <div className={'carousel'}>
                <div className={'badges'}>
                    {
                        badges ? badges.names.map(b => {
                            return <div className={'badgeItem'}
                                        style={{backgroundColor:
                                                b === 'Акция' ? 'limegreen' : b === 'Хит'
                                                    ? 'crimson' : b === 'Советуем' ? 'darkorange' : 'darkcyan'}}>{b}</div>
                        }) : null
                    }
                </div>
                <Slider {...settings}>
                    { images ?
                        images.map(i => {
                       return <div className={'carousel__item'}>
                            <img src={i} className={styles.imgGallery}/>
                        </div>
                    })
                        : <Preloader />
                    }
                </Slider>
            </div>
        );
    }
}


