import React from 'react';
import styles from './../Card.module.scss'


const SuccessSendData = (props) => {
    const {trackNumber, isAuth} = props;

    return (
        <div className={styles.resultCard}>
            <div className={styles.resultCard__counts}>
                Совет
            </div>
            <div className={styles.resultCard__price}>
                Авторизируйтесь на сайте, чтобы иметь доступ к разделу <span>"Мои заказы"</span>.
                Неавторизированные пользователи смогут отследить заказ по трек-номеру
            </div>
            <button className={styles.button}>
                <i className="large material-icons">check</i> Авторизация
            </button>
        </div>
    )
}

export default SuccessSendData;