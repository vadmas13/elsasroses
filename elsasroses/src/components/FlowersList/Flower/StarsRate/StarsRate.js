import React from 'react';
import style from './../../FlowersList.module.scss'

const StarsRate = (props) => {
    let arrayRate = [...Array(5)];
    return (
        <>
            <div className={style.stars}>
                { arrayRate.map((n, i) => <i className={props.rate > i  ? style.stars__item + " small material-icons"
                    : " small material-icons"}>star</i>)}

            </div>
        </>
    )
}

export default StarsRate;