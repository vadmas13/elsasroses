import React from 'react';
import style from './../FlowersList.module.scss'
import StarsRate from "./StarsRate/StarsRate";
import {NavLink} from "react-router-dom";

const Flower = (props) => {

    const {flowers} = props;
    let compareFLower = (e) => {
        let f = parseInt(e.currentTarget.id);
        props.newIdFlowerForCompare(f)
    }
    let sympathyFlower = (e) => {
        let f = parseInt(e.currentTarget.id);
        props.newIdFlowerForSympathy(f);
    }
    let cardFlower = (e) => {
        let f = parseInt(e.currentTarget.id);
        props.newIdFlowerForCard(f);
    }

    let deleteFlower = (e) => {
        e.preventDefault();
        let fId = parseInt(e.currentTarget.id);
        props.deleteFlowerFromState(fId, props.typeState);
    }

    return (
        <>
            {flowers.map(f => {
                if (f) {
                    return (
                        <div className={style.cardFlower}>

                            <div className={style.cardImage}>
                                { props.typeState ?
                                    <a onClick={deleteFlower} className={style.close} id={f.id}><i className="material-icons">close</i></a>
                                    :null
                                }
                                <NavLink to={`/cart/${f.id}`} className={style.cardImage__order}>
                                <div className={style.badges}>
                                    {
                                        f.badges && f.badges.map(b => {
                                            return <div className={style.badgeItem}
                                                        style={{backgroundColor:
                                                                b === 'Акция' ? 'limegreen' : b === 'Хит'
                                                                    ? 'crimson' : b === 'Советуем' ? 'darkorange' : 'darkcyan'}}>{b}</div>
                                        })
                                    }
                                </div>
                                <div className={style.cardImage__showCard}>
                                    <div className={style.cardImage__orderIcon}>
                                        <i className={" small material-icons"}>remove_red_eye</i>
                                    </div>
                                </div>
                                <img src={f.linkImg} alt={f.title}/>
                                </NavLink>
                            </div>
                            <div className={style.cardContent}>
                                <StarsRate rate={f.rate}/>
                                <span className={style.cardTitle}>{f.title}</span>
                                <div className={style.price}>
                                    <div className={style.priceAndColors}>
                                        <span>{f.price} ₽ </span>
                                        <div className={style.colors}>
                                            {f.colors && f.colors.length !== 0 && f.colors[0] !== '' ?
                                                f.colors.map(c => <div className={style.colorsItem}>
                                                    <i style={{color: c}} className="material-icons">local_florist</i>
                                                </div>) : null}
                                        </div>
                                    </div>
                                </div>
                                <div className={style.panel}>
                                    <a onClick={cardFlower} id={f.id}>
                                        {f.buttons.card ?
                                            <button className={`${style.addToCart} ${style.addToCart__success}`}>
                                                <div className={style.addToCart__active}>
                                                    <span>В корзине</span>
                                                    <i className={"small material-icons"}>check_circle</i>
                                                </div>
                                                <div className={style.addToCart__disabled}>
                                                    <span>Убрать</span>
                                                    <i className={"small material-icons"}>delete_sweep</i>
                                                </div>
                                            </button>
                                            :
                                            <button className={`${style.addToCart}`}>
                                                <div className={`${style.addToCart__default}`}>
                                                    <span>В корзину</span>
                                                    <i className={"small material-icons"}>add_shopping_cart</i>
                                                </div>
                                            </button>
                                        }

                                    </a>
                                    { props.typeState !== 'sympathy' ?
                                    <div className={style.iconsPanel}>
                                        <a onClick={sympathyFlower} id={f.id}
                                           className={f.buttons.sympathy ? style.active : ''}>
                                            <i className="small material-icons">favorite</i>
                                        </a>
                                    </div> : null
                                    }
                                    <div className={style.iconsPanel}>
                                        <a onClick={compareFLower} id={f.id}
                                           className={f.buttons.compare ? style.active : ''}>
                                            <i className="small material-icons">shuffle</i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                }
            })}
        </>
    )
}

export default Flower;