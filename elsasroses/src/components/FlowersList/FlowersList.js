import React from 'react';
import Flower from "./Flower/Flower";
import {compose} from "redux";
import {getFlowersForList} from "../../hoc/getFlowersForList";


const FlowersList = (props) => {
    const {flowers} = props;
    return (
        <div className={'wrapperFlowers'}>
            <div className={'containerFlowers'}>
                { flowers && flowers.length ? <Flower flowers={flowers}
                                           newIdFlowerForCompare={props.newIdFlowerForCompare}
                                           newIdFlowerForSympathy={props.newIdFlowerForSympathy}
                                           newIdFlowerForCard={props.newIdFlowerForCard}
                /> : <p>Нет цветов</p>}
            </div>
        </div>
    )
}



export default getFlowersForList(null, FlowersList);