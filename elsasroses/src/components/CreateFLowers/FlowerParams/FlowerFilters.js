import React from 'react'
import Preloader from "../../common/Preloader/Preloader";
import styles from './../CreateFlowers.module.scss'

const FlowerFilters = (props) => {
    const {checkedFilters, addFiltersData, deleteFiltersData, createImagesAndPrices} = props;


    let addFilters = (e) => {
        let elemId = e.currentTarget.id;
        addFiltersData(elemId);
    }

    let deleteFilters = (e) => {
        let elemId = e.currentTarget.id;
        let title = elemId.split('_');
        deleteFiltersData(title[1]);
    }

    let successFilters = () => {
        createImagesAndPrices()
    }

    return (
        <form onSubmit={props.handleSubmit} className="white">
            {checkedFilters ?
                <div>
                    <div>
                        {
                            checkedFilters.stageFiltersCategory.stage <= 3 ?
                                <div>
                                    <p>Добавить фильтр - {checkedFilters.stageFiltersCategory.stageTitle} категория</p>
                                    <a className="btn-floating btn-large waves-effect waves-light red"
                                       id={checkedFilters.stageFiltersCategory.name}
                                       onClick={addFilters}>
                                        <i className="material-icons">add</i>
                                    </a>
                                </div>
                                : <div>Максимальное число фильтров, больше нельзя</div>
                        }
                    </div>
                    <div>
                        {checkedFilters.levels ?
                            Object.keys(checkedFilters.levels).map((key, i) => {
                                return (
                                    <div>
                                        <div className={styles.itemHeader}>
                                            <div className={styles.itemCounter}>{i + 1}</div>
                                            <a className={styles.itemDelete} id={`delete_${key}`}
                                               onClick={deleteFilters}>Удалить</a>
                                        </div>
                                        <div className="input-field">
                                            <label htmlFor={`name_${key}_${i}`}>Название фильтра</label>
                                            <input type="text" id={`name_${key}_${i}`} className={'validate'}
                                                   onChange={props.handleChange}/>
                                        </div>
                                        <div className="input-field">
                                            <label htmlFor={`titles_${key}_${i}`}>Перечисли через запятую заголовки
                                                фильтра</label>
                                            <input type="text" id={`titles_${key}_${i}`} className={'validate'}
                                                   onChange={props.handleChange}/>
                                        </div>
                                    </div>
                                )
                            }) : null
                        }
                        <p>Утверди фильтры, чтобы заполнить информацию о ценах и изображениях на них</p>
                        <a className={`${styles.itemDelete} ${styles.itemSuccess}`} id={`success`}
                           onClick={successFilters}>
                            Утвердить фильтры
                        </a>
                    </div>

                    {
                        checkedFilters.multiParams ?
                            <div>
                                {Object.keys(checkedFilters.multiParams.imagesFilters.images).map((imageTitles, i) => {
                                    let titles = imageTitles.split('_');
                                    return (
                                        <div className={styles.filters}>
                                            <div className={styles.filters__item}>
                                                {titles.map(val => (
                                                    <div className={`chip ${styles.filters__chip}`}>{val}</div>
                                                ))}
                                            </div>
                                            <div className="input-field">
                                                <label htmlFor={`imagesFilters-${imageTitles}_${0}`}>Сылка на изображение</label>
                                                <input type="text" id={`imagesFilters-${imageTitles}_${0}`} className={'validate'}
                                                       onChange={props.handleChange}/>
                                            </div>
                                            <div className="input-field">
                                                <label htmlFor={`imagesFilters-${imageTitles}_${1}`}>Сылка на изображение</label>
                                                <input type="text" id={`imagesFilters-${imageTitles}_${1}`} className={'validate'}
                                                       onChange={props.handleChange}/>
                                            </div>
                                            <div className="input-field">
                                                <label htmlFor={`priceFilters-${imageTitles}`}>Цена</label>
                                                <input type="text" id={`priceFilters-${imageTitles}`} className={'validate'}
                                                       onChange={props.handleChange}/>
                                            </div>
                                        </div>
                                    )
                                })}
                            </div>
                            : null
                    }
                    <div className="input-field">
                        <button className="btn pink lighten-1 z-depth-0" onClick={props.handleSubmit}>Далее</button>
                    </div>
                </div>
                : <Preloader/>}

        </form>
    )
}

export default FlowerFilters