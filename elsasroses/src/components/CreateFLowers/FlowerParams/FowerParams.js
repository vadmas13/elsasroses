import React from 'react'

const FLowerParams = (props) => {
    const {title} = props;
    return (
            <form onSubmit={props.handleSubmit} className="white">

                <div className="input-field">
                    <label htmlFor="gipercium">Гиперкиум</label>
                    <input type="text" id="gipercium" onChange={props.handleChange}/>
                </div>
                <div className="input-field">
                    <label htmlFor="lenta">Лента</label>
                    <input type="text" id="lenta" onChange={props.handleChange}/>
                </div>
                <div className="input-field">
                    <label htmlFor="size">Размер</label>
                    <input type="text" id="size" onChange={props.handleChange}/>
                </div>
                <div className="input-field">
                    <label htmlFor="rose">Роза</label>
                    <input type="text" id="rose" onChange={props.handleChange}/>
                </div>
                <div className="input-field">
                    <label htmlFor="sizal">Сизаль</label>
                    <input type="text" id="sizal" onChange={props.handleChange}/>
                </div>
                <div className="input-field">
                    <label htmlFor="sortBy">Сортировка</label>
                    <input type="text" id="sortBy" onChange={props.handleChange}/>
                </div>
                <div className="input-field">
                    <label htmlFor="type">Тип</label>
                    <input type="text" id="type" onChange={props.handleChange}/>
                </div>
                <div className="input-field">
                    <label htmlFor="fistashka">Фисташки Зелень</label>
                    <input type="text" id="fistashka" onChange={props.handleChange}/>
                </div>
                <div className="input-field">
                    <label htmlFor="sharChristmas">Шар Новогодний</label>
                    <input type="text" id="sharChristmas" onChange={props.handleChange}/>
                </div>

                <div className="input-field">
                    <button className="btn pink lighten-1 z-depth-0" onClick={props.handleSubmit}>Далее</button>
                </div>
            </form>
    )
}

export default FLowerParams