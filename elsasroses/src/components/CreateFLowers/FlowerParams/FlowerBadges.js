import React from 'react'
import Preloader from "../../common/Preloader/Preloader";

const FlowersBadges = (props) => {
    const {badges} = props;

    if (badges && badges.length !== 0) {
        return (
            <form onSubmit={props.handleSubmit} className="white">
                {badges[0].names.map(b => {
                    return <p>
                        <label>
                            <input type="checkbox" id={b} onClick={props.handleChange}/>
                            <span>{b}</span>
                        </label>
                    </p>
                })}
                <div className="input-field">
                    <button className="btn pink lighten-1 z-depth-0" onClick={props.handleSubmit}>Сохранить</button>
                </div>
            </form>


        )
    } else return <Preloader/>

}

export default FlowersBadges