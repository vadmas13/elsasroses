import React from 'react'
import styles from './../CreateFlowers.module.scss'

const FlowersInfo = (props) => {
    const {flowersColors, colorsState, oldPrice} = props;

    let changeStateColors = (e) => {
        let color = e.currentTarget.id;
        props.changeColors(color);
    }

    return (
        <form onSubmit={props.handleSubmit} className="white">

            <div className="input-field">
                <label htmlFor="title">Название букета</label>
                <input type="text" id="title" className={'validate'} onChange={props.handleChange}/>
            </div>
            <div className="input-field">
                <label htmlFor="content">Описание букета</label>
                <textarea id="content" onChange={props.handleChange} className='materialize-textarea'></textarea>
            </div>
            <div className="input-field">
                <label htmlFor="price">Цена (Для мини карточки)</label>
                <input type="text" id="price" className={'validate'} onChange={props.handleChange}/>
            </div>
            <div className="input-field">
                <label htmlFor="sale">Процент скидки (Для мини карточки <span className={styles.oldPrice}>{oldPrice ? ', старая цена: ' + oldPrice : null})</span></label>
                <input type="text" id="sale" className={'validate'} onChange={props.handleChange}/>
            </div>
            <div className="input-field">
                <label htmlFor="linkImg">Сылка на изображение (Для мини карточки)</label>
                <input type="text" id="linkImg" className={'validate'} onChange={props.handleChange}/>
            </div>
            <div className={styles.colors}>
                <p>Укажи цвета букета</p>
                <div className={styles.colorsContainer}>
                    {
                        flowersColors ? flowersColors[0].colors.map(color => {
                            return <a onClick={changeStateColors}
                                      className={colorsState.some(c => c === color) ? styles.colorItem + ' ' + styles.colorSuccess : styles.colorItem}
                                      style={{backgroundColor: color}} id={color}></a>
                        }) : null
                    }
                </div>
                <div className={styles.textCount}>Выбрано цветов: { colorsState.length }</div>
            </div>
            <div className="input-field">
                <button className="btn pink lighten-1 z-depth-0" onClick={props.handleSubmit}>Далее</button>
            </div>
        </form>
    )
}

export default FlowersInfo