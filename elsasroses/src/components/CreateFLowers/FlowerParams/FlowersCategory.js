import React from 'react'

const FlowersCategory = (props) => {
    const {categories} = props;
    return (
        <div>
            <form onSubmit={props.handleSubmit} className="white">
                {categories.map(c => {
                    if(!c.categories) return false;
                    let category = c.categories;
                    let keys = Object.keys(category);
                    let values = Object.values(category);
                    let elements = keys.map((k, i) => {
                        return (
                            <p>
                                <label>
                                    <input type="checkbox" id={k} onClick={props.changeChooseCategories}/>
                                    <span>{values[i]}</span>
                                </label>
                            </p>
                        )
                    })
                    return (
                        <div>
                            <h5> {c.name} </h5>
                            <div> {elements} </div>
                        </div>
                    )
                })}

                <div className="input-field">
                    <button className="btn pink lighten-1 z-depth-0" onClick={props.handleSubmit}>Сохранить</button>
                </div>
            </form>
        </div>
    )
}

export default FlowersCategory