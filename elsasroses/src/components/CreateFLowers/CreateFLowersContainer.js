import React, {Component} from 'react'
import CreateFLowers from "./CreateFLowers";
import {connect} from 'react-redux'
import {addFlowerFilters, addNewFlowerInfo, addNewFlowerParams, setFlowerBadges} from "../../redux/actions/actionsCreateFlowers";
import {compose} from "redux";
import {firestoreConnect} from "react-redux-firebase";
import {setFlowerCategories} from "../../redux/categoryReducer";

class CreateFLowersContainer extends Component {

   render(){
       return(
           <CreateFLowers stage={this.props.stage}
                          categories={this.props.categories}
                          lastArticle = {this.props.lastArticle}
                          flowersDictionary = {this.props.flowersDictionary}
                          addFlowerInfo={this.props.addNewFlowerInfo}
                          addFlowerParams={this.props.addNewFlowerParams}
                          setFlowerCategories={this.props.setFlowerCategories}
                          addFlowerFilters={this.props.addFlowerFilters}
                          badges={this.props.badges}
                          setFlowerBadges={this.props.setFlowerBadges}
                          flowersColors={this.props.flowersColors}
           />
       )
   }
}


const mapStateToProps = (state) => {
    return{
        stage: state.createFlower,
        lastArticle: state.firestore.ordered.lastArticle,
        flowersDictionary: state.firestore.ordered.flowersDictionary,
        badges: state.firestore.ordered.badges,
        categories: state.categories,
        flowersColors: state.firestore.ordered.flowersColors
    }
}


export default compose(
    connect(mapStateToProps, {addNewFlowerInfo, addNewFlowerParams, setFlowerCategories, addFlowerFilters, setFlowerBadges}),
    firestoreConnect([
        {collection: "lastArticle"},
        {collection: "flowersDictionary"},
        {collection: "badges", doc: 'default'},
        {collection: "flowersColors", doc: 'default'}
    ])
)(CreateFLowersContainer);