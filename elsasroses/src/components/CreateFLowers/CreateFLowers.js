import React, {Component} from 'react';
import FlowersInfo from "./FlowerParams/FlowersInfo";
import FLowerParams from "./FlowerParams/FowerParams";
import Preloader from "../common/Preloader/Preloader";
import FlowersCategory from "./FlowerParams/FlowersCategory";
import {Redirect} from "react-router-dom";
import StepsOfCreating from "./StepsOfCreating/StepsOfCreating";
import FlowerFilters from "./FlowerParams/FlowerFilters";
import FlowerBadges from "./FlowerParams/FlowerBadges";


class CreateFLowers extends Component {

    state = {
        stage: 1,
        loading: false,
        colors: [],
        title: '',
        price: '',
        oldPrice: '',
        linkImg: '',
        article: '',
        lastArticle: null,
        params: {
            gipercium: '',
            lenta: '',
            size: '',
            rose: '',
            sizal: '',
            sortBy: '',
            type: '',
            fistashka: '',
            sharChristmas: ''
        },
        categories: null,
        checkedFilters: {
            stageFiltersCategory: {stageTitle: 'Первая', name: 'firstlvlFilters', stage: 1}
        },
        badges: {}
    }


    addFiltersData(name) {
        let filters = this.state.checkedFilters;
        let filterCategory = this.state.checkedFilters.stageFiltersCategory;
        let newName = '';

        if (filters[name]) {
            if (name !== 'firstlvlFilters') newName = 'firstlvlFilters';
            else if (name !== 'secondlvlFilters') newName = 'secondlvlFilters';
            else if (name !== 'thirdlvlFilters') newName = 'thirdlvlFilters';
        } else {
            newName = name;
        }

        this.setState({
            checkedFilters: {
                ...filters, levels: {...filters.levels, [newName]: {}},
                stageFiltersCategory: {
                    ...filterCategory,
                    stageTitle: filterCategory.stageTitle === 'Первая' ? 'Вторая' : 'Третья',
                    stage: filterCategory.stage + 1,
                    name: filterCategory.name === 'firstlvlFilters' ? 'secondlvlFilters' : 'thirdlvlFilters'
                }
            }
        });
    }

    changeColors(color) {
        let colors = this.state.colors;
        let filterColors = colors.filter(c => c !== color);
        if (colors.length === filterColors.length || colors.length === 0) filterColors.push(color);
        this.setState({
            colors: [...filterColors]
        })
    }

    createImagesAndPrices() {

        const checkedFilters = this.state.checkedFilters;

        let setStateImagesAndPrices = (checkedFilters, sortTitles, field) => {
            let objectTitlesPrice = {};
            let objectTitlesImages = {};


            sortTitles.forEach((val, i) => {
                objectTitlesPrice = {...objectTitlesPrice, [val]: ''};
                objectTitlesImages = {...objectTitlesImages, [val]: []};
            })

            this.setState({
                checkedFilters: {
                    ...checkedFilters, multiParams:
                        {
                            imagesFilters: {images: {...objectTitlesImages}},
                            priceFilters: {price: {...objectTitlesPrice}}
                        }
                }
            })

        }

        let filters = {...checkedFilters.levels};
        delete filters.stageFiltersCategory;
        delete filters.multiParams;

        let titlesArrays = Object.keys(filters).map((f, i) => {

            if (filters[f] && filters[f]['titles']) {
                let titles = filters[f]['titles'];
                return titles.split(',');
            }
        })

        let sortTitles = [];

        if (titlesArrays[0]) {
            titlesArrays[0].map((itemTitlesFirst, i) => {
                if (titlesArrays[1]) {
                    titlesArrays[1].map((itemTitlesSecond) => {
                        if (titlesArrays[2]) {
                            titlesArrays[2].map((itemTitlesThird) => {
                                return sortTitles.push(itemTitlesFirst.trim() + '_' + itemTitlesSecond.trim() + '_' + itemTitlesThird.trim());
                            })
                        } else {
                            return sortTitles.push(itemTitlesFirst.trim() + '_' + itemTitlesSecond.trim());
                        }
                    })
                } else {
                    return sortTitles.push(itemTitlesFirst.trim());
                }
            })
        }
        setStateImagesAndPrices(checkedFilters, sortTitles, 'images');

        return sortTitles;

    }

    deleteFiltersData(name) {
        let stageFiltersCategory = this.state.checkedFilters.stageFiltersCategory;
        let filters = {...this.state.checkedFilters.levels};
        delete filters[name];

        this.setState({
            checkedFilters: {
                ...filters, stageFiltersCategory: {
                    ...stageFiltersCategory,
                    stage: stageFiltersCategory.stage - 1,
                    stageTitle: stageFiltersCategory.stageTitle === 'Третья' ? 'Вторая' : 'Первая',
                    name: name
                }
            }
        });
    }

    changeFiltersData(field, name, value) {
        let {checkedFilters} = this.state;
        let r = {...checkedFilters};

        Object.keys(checkedFilters.levels).forEach((key, i) => {
            if (key === name) {
                r = {
                    ...r,
                    levels: {...checkedFilters.levels, [key]: {...checkedFilters.levels[key], [field]: value}}
                }
            }
        });

        return r;
    }

    changeMultiParamsData(field, titles, value, i) {
        let typeField, setValue = [], {checkedFilters} = this.state;

        if (field === 'imagesFilters') {
            typeField = 'images';
            checkedFilters.multiParams[field][typeField][titles][i] = value;
        } else {
            typeField = 'price';

            checkedFilters.multiParams[field][typeField][titles] = value;
        }

        return checkedFilters;

    }

    transformArray(array) {
        let newObj = {};
        array.map(el => {
            if (!el.components) return false;
            let keys = Object.keys(el.categories);
            let values = Object.values(el.categories); // fuck!
            keys.map((k, i) => {
                newObj = {...newObj, [k]: false}
            })
        });
        return newObj;
    }


    componentDidUpdate(prevProps, prevState) {

        if (this.props.lastArticle !== prevProps.lastArticle) this.setState({lastArticle: parseInt(this.props.lastArticle[0].article)});

        if (this.props.categories.length !== prevProps.categories.length) this.setState({categories: this.transformArray(this.props.categories)});

        if (this.props.stage.stage !== 1 && this.state.loading === true && this.props.stage.stage !== prevProps.stage.stage) {
            this.setState({
                loading: false,
                stage: this.props.stage.stage
            });
        }

    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({loading: true});
        switch (this.state.stage) {
            case 1:
                return this.props.addFlowerInfo(this.state);
            case 2:
                return this.props.addFlowerFilters(this.state);
            case 3:
                return this.props.addFlowerParams(this.state);
            case 4:
                return this.props.setFlowerCategories(this.state);
            case 5:
                return this.props.setFlowerBadges(this.state);
        }
    }

    handleChange = (e) => {
        if (this.state.stage === 1) {
            let field = e.target.id;
            let value  = e.target.value;
            if (field === 'sale') {
                let price = this.state.price;
                let oldPrice = Math.floor(price - (value * price / 100));
                this.setState({
                    [field]: value, oldPrice
                })
            } else {
                this.setState({
                    [field]: value
                })
            }
        } else if (this.state.stage === 2) {
            let checkedFiltersData;
            let params = e.target.id;
            let value = e.currentTarget.value;
            let dataFilters = params.split('_');
            let dataMultiParams = params.split('-');
            if (dataMultiParams.length > 1) {
                let titles = dataMultiParams[1].split('_');
                checkedFiltersData = this.changeMultiParamsData(dataMultiParams[0], titles[0], value, parseInt(titles[1]));
            }
            else {
                checkedFiltersData = this.changeFiltersData(dataFilters[0], dataFilters[1], value);
            }

            this.setState({
                checkedFilters: checkedFiltersData
            })


        } else if (this.state.stage === 3) {
            this.setState({
                params: {...this.state.params, [e.target.id]: e.target.value}
            })
        } else if (this.state.stage === 5) {
            let el = e.currentTarget;
            this.setState({
                badges: {...this.state.badges, [el.id]: el.checked}
            })
        }
    }


    stepsStages() {
        if (this.state.stage === 1)
            return {
                first: {i: 'refresh', className: 'active'},
                second: 'close',
                third: 'close',
                fourth: 'close',
                five: 'close'
            }
        else if (this.state.stage === 2)
            return {
                first: 'check',
                second: {i: 'refresh', className: 'active'},
                third: 'close',
                fourth: 'close',
                five: 'close'
            }
        else if (this.state.stage === 3)
            return {
                first: 'check',
                second: 'check',
                third: {i: 'refresh', className: 'active'},
                fourth: 'close',
                five: 'close'
            }
        else if (this.state.stage === 4)
            return {
                first: 'check',
                second: 'check',
                third: 'check',
                fourth: {i: 'refresh', className: 'active'},
                five: 'close'
            }
        else if (this.state.stage === 5)
            return {
                first: 'check',
                second: 'check',
                third: 'check',
                fourth: 'check',
                five: {i: 'refresh', className: 'active'}
            }

    }

    changeChooseCategories(e) {
        let el = e.currentTarget;
        this.setState({
            categories: {...this.state.categories, [el.id]: el.checked}
        })
    }

    render() {
        const {stage} = this.state;

        if (stage === 6) return <Redirect to={`/cart/${this.state.lastArticle}`}/>
        return (
            <div className="container">
                <h3 className="grey-text text-darken-3">Добавить новый букет</h3>
                <StepsOfCreating stage={stage} stepsStages={this.stepsStages.bind(this)}/>

                {
                    !this.state.loading ? stage === 1 ?
                        <FlowersInfo handleSubmit={this.handleSubmit} handleChange={this.handleChange}
                                     changeColors={this.changeColors.bind(this)}
                                     oldPrice={this.state.oldPrice}
                                     colorsState={this.state.colors}
                                     flowersColors={this.props.flowersColors}/> :
                        stage === 2 ?
                            <FlowerFilters handleSubmit={this.handleSubmit}
                                           addFiltersData={this.addFiltersData.bind(this)}
                                           checkedFilters={this.state.checkedFilters}
                                           deleteFiltersData={this.deleteFiltersData.bind(this)}
                                           createImagesAndPrices={this.createImagesAndPrices.bind(this)}
                                           handleChange={this.handleChange}/>
                            :
                            stage === 3 ?
                                <FLowerParams handleSubmit={this.handleSubmit}
                                              handleChange={this.handleChange}
                                              title={this.state.title}/> :
                                stage === 4 ?
                                    <FlowersCategory
                                        changeChooseCategories={this.changeChooseCategories.bind(this)}
                                        categories={this.props.categories}
                                        handleSubmit={this.handleSubmit}
                                    /> :
                                    <FlowerBadges badges={this.props.badges} handleSubmit={this.handleSubmit}
                                                  handleChange={this.handleChange}/>
                        :
                        <Preloader/>
                }

            </div>
        )
    }
}

export default CreateFLowers;