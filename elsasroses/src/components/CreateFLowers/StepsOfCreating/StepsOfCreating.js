import React from 'react';
import styles from './StepsOfCreating.module.scss'

const StepsOfCreating = (props) => {
    const info = props.stepsStages();

    return(
        <div className={styles.progress}>
            <ul>
                <li className={styles[info.first.className]}>
                    <h6><span>1.</span> Общая информация</h6>
                    <i className="material-icons">{info.first.i || info.first}</i>
                </li>
                <li className={styles[info.second.className]}>
                    <h6><span>2.</span> Фильтры букета</h6>
                    <i className="material-icons">{info.second.i || info.second}</i>
                </li>
                <li className={styles[info.third.className]}>
                    <h6><span>3.</span> Параметры букета</h6>
                    <i className="material-icons">{info.third.i || info.third}</i>
                </li>
                <li className={styles[info.fourth.className]}>
                    <h6><span>4.</span> Категории для букета</h6>
                    <i className="material-icons">{info.fourth.i || info.fourth}</i>
                </li>
                <li className={styles[info.five.className]}>
                    <h6><span>5.</span>Бейджики </h6>
                    <i className="material-icons">{info.five.i || info.five}</i>
                </li>
            </ul>
        </div>
    )
}


export default StepsOfCreating;
