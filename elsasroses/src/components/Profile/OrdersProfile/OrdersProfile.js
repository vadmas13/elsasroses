import React, {useState} from 'react'
import styles from './../Profile.module.scss'
import TrackOrderSuccess from "../../TrackOrder/TrackOrderSuccess/TrackOrderSuccess";



const OrdersProfile = (props) => {
    const {orders, requestOrder} = props;

    let resultPrice = (count, price) => {
        return parseInt(count) * parseInt(price.replace(' ', ''));
    }

    let [focusOrder, setFocusOrder] = useState();

    let FOUND_REQUEST_ORDER = false;

    let handleChange = (e) => {
        let value = e.target.value;
        props.getRequestOrder(value);
    }

    return (
        <div>
            <div className={styles.OrderProfile}>
                <h6 className={styles.Profile__titleOrderCard}><i
                    className="small material-icons">playlist_add_check</i> Мои заказы</h6>
                <div className={`input-field col s6 ${styles.OrderProfile__input}`}>
                    <i className="material-icons prefix">search</i>
                    <input id="icon_prefix" type="text" className="validate" value={requestOrder} onChange={handleChange}/>
                        <label htmlFor="icon_prefix">Трек номер</label>
                </div>
            </div>
            {
                orders.map((order, i) => {
                    let trackOrder = {
                        trackNumber: order.trackNumber.toString(),
                        createdAt: order.createdAt,
                        status: order.status,
                        flowers: order.flowers,
                        focus: focusOrder === order.trackNumber
                    }

                    if(requestOrder) {
                        if(trackOrder.trackNumber.indexOf(requestOrder) !== -1) {
                            FOUND_REQUEST_ORDER = true;
                            return <TrackOrderSuccess trackOrder={trackOrder}
                                                      order={order}
                                                      statusOrder={props.statusOrder}
                                                      setFocusOrder={setFocusOrder}
                                                      resultPrice={resultPrice}/>;
                        }
                        if(!FOUND_REQUEST_ORDER && orders.length === i+1) return <p>Нет совпадений</p>
                    }else return <TrackOrderSuccess trackOrder={trackOrder}
                                                    order={order}
                                                    statusOrder={props.statusOrder}
                                                    setFocusOrder={setFocusOrder}
                                                    resultPrice={resultPrice}/>
                })
            }
        </div>
)
}

export default OrdersProfile;