import React from 'react'
import styles from './Profile.module.scss'
import OrdersProfile from "./OrdersProfile/OrdersProfile";
import {getRequestOrder} from "../../redux/profileReducer";
import ProfileInfo from "./ProfileInfo/ProfileInfo";


const Profile = (props) => {
    const {user, profile} = props;

    return(
        <div className={styles.Profile}>
           <ProfileInfo user={user}/>
            { profile.orders.length !== 0 ?
            <div className={styles.Profile__Orders}>
                <OrdersProfile orders={profile.orders}  statusOrder={props.statusOrder}
                               requestOrder={profile.requestOrder} getRequestOrder={props.getRequestOrder}/>
            </div>
             : null }
        </div>
    )
}

export default Profile;