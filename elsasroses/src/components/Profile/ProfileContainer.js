import React from 'react'
import Profile from "./Profile";
import {compose} from "redux";
import {getProfileData} from "../../hoc/getProfileData";


const ProfileContainer = (props) => {
    const {user, profile} = props;

    return <Profile user={user} statusOrder={props.statusOrder}
                    profile={profile} getRequestOrder={props.getRequestOrder}/>
}

export default compose(
    getProfileData
)(ProfileContainer);