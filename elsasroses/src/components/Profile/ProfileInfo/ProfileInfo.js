import React from 'react'
import styles from './../Profile.module.scss'

const ProfileInfo = (props) => {
    const {user} = props;

    return(
            <div className={styles.Profile__info}>
                <i className="small material-icons">account_circle</i>
                <p>{ user.name }</p>
            </div>
    )
}

export default ProfileInfo;