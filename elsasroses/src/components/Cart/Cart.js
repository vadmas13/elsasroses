import React from 'react';
import styles from './Cart.module.scss'
import Counter from "./counter/counter";
import Gallery from "./gallegy/Gallery";
import Filters from "./filters/Filters";
import Price from "./price/Price";
import Preloader from "../common/Preloader/Preloader";
import Charastericstics from "./charastericstics/Charastericstics";
import CartHeader from "./cartHeader/CartHeader";
import Reviews from "./reviews/Reviews";
import {addReview} from "../../redux/actions/actionReviews";
import ResultPrice from "./ResultPrice/ResultPrice";

const Cart = (props) => {
    const {
        counter, filterTitle, setFilterTitle, images, price, setTitles, filters,
        newIdFlowerForSympathy, newIdFlowerForCompare, cartStorage, changeButtonsCart, changeButtonCardWhenChangeCounts
    } = props;
    const {flowerInfo, flowerParams, badges, paramsDefault} = props.flower;


    if (flowerInfo.length !== 0) {
        return (
            <div className={'wrapperFlowers'}>
                <h1 className={styles.title}>{flowerInfo.title}</h1>
                <div className={'containerFlowers'}>
                    <div className={styles.cardContainer}>
                        <div className={styles.cardContainer__pictures}>
                            <Gallery badges={badges} images={images}/>
                        </div>
                        <div className={styles.cardContainer__info + ' ' + styles.info}>

                            <CartHeader id={flowerInfo.id} newIdFlowerForSympathy={newIdFlowerForSympathy}
                                        newIdFlowerForCompare={newIdFlowerForCompare}
                                        compare={cartStorage ? cartStorage.compare : null}
                                        changeButtonsCart={changeButtonsCart}
                                        sympathy={cartStorage ? cartStorage.sympathy : null}
                            />

                            <hr/>

                            <Charastericstics flowerParams={flowerParams} paramsDefault={paramsDefault}/>

                            <Price price={price}/>

                            <Filters setTitles={setTitles} filterTitle={filterTitle} filters={filters}
                                     setFilterTitle={setFilterTitle}/>

                            <Counter counter={counter}
                                     changeButtonsCart={changeButtonsCart}
                                     filterTitle={filterTitle}
                                     changeButtonCardWhenChangeCounts={changeButtonCardWhenChangeCounts}
                                     id={flowerInfo.id} added={cartStorage ? cartStorage.card.added : null}/>
                            { counter.count > 1 && <ResultPrice resultPrice={props.resultPrice}/> }

                        </div>
                    </div>
                    <div className={styles.cardContainer__reviews}>
                        <Reviews newReview={props.newReview}
                                 reviewsList={props.reviewsList}
                                 id={flowerInfo.id}
                                 auth={props.auth}
                                 addReview={props.addReview}
                                 content={flowerInfo.content}
                                 changeRviewsData={props.changeRviewsData} />
                    </div>
                </div>
            </div>
        )
    } else return <Preloader/>
}

export default Cart;