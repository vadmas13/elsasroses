import React, {useState, useEffect} from 'react';
import {compose} from "redux";
import Cart from "./Cart";
import separateDataFilters from "../../hoc/separateDataFilters";
import Card from "../Card/Card";
import OrderInfo from '../Card/OrderCard/OrderInfo'
import {addReview} from "../../redux/actions/actionReviews";


const CartContainer = (props) => {

    const {
        flower, changeButtonsCart, changeButtonCardWhenChangeCounts, isCard, setCurrentPrice,
        changeResultPrice, deleteFlowerFromCard, orderCard, addFlowerInOrder
    } = props;

    const {firstlvlFilters, secondlvlFilters, thirdlvlFilters, imagesFilters, priceFilters, cartStorage} = props;
    const filters = [firstlvlFilters, secondlvlFilters, thirdlvlFilters];
    let countInit = cartStorage ? cartStorage.card.count : null;
    let resultPriceState = cartStorage ? cartStorage.resultPrice : null;
    let storageFilterTitle = cartStorage.card.filterTitle;

    let [count, setCount] = useState(countInit);
    let [images, setImages] = useState();
    let [price, setPrice] = useState();
    let [resultPrice, setResultPrice] = useState(resultPriceState);
    let [useFilters, getUseFilters] = useState(false);


    const setInitialTitle = (lvlFilters, filterTitleStorage) => {
        if (!filterTitleStorage) {
            let filterTitle = '';

            lvlFilters.forEach(f => {
                if (f && f.titles.length) filterTitle += f.titles[0] + '_';
            });

            return filterTitle.slice(0, -1);
        } else {
            return filterTitleStorage;
        }
    }


    const setTitles = (filterTitle, title, i) => {
        let titles = filterTitle.split('_');
        let arrayNewTitles = titles.map((t, n) => {
            if (n === parseInt(i)) return title;
            else return t;
        });
        return arrayNewTitles.join('_');
    }

    let [filterTitle, setFilterTitle] = useState(setInitialTitle([firstlvlFilters, secondlvlFilters, thirdlvlFilters], storageFilterTitle));


    const setFilter = (flower, FieldFilter, filterTitle, typeFilter) => {

        let data = FieldFilter.price ? FieldFilter.price : FieldFilter.images;
        if (filterTitle && data && flower.flowerInfo.id === FieldFilter.id) {
            return data[filterTitle]
        }
        else if (typeFilter === 'images') {
            return [flower.flowerInfo.linkImg]
        }
        else {
            return flower.flowerInfo.price
        }
    }

    useEffect(() => {
        storageFilterTitle && setFilterTitle(storageFilterTitle)
    }, [storageFilterTitle])

    useEffect(() => { debugger
        setImages(setFilter(flower, imagesFilters, filterTitle, 'images'));
        setPrice(setFilter(flower, priceFilters, filterTitle));
        setCount(countInit);
    }, [filterTitle, countInit, flower.flowerInfo]);

    useEffect(() => {
        addFlowerInOrder(flower.flowerInfo.id, count, flower.flowerInfo.title, filterTitle, price, useFilters);
    }, [useFilters, price, count]);


    useEffect(() => {
        if (price) {
            let changePrice = parseInt(price) * parseInt(count);
            changeResultPrice(flower.flowerInfo.id, changePrice);
            setCurrentPrice(flower.flowerInfo.id, price);
            setResultPrice(changePrice);
        }
    }, [price, count]);


    if (isCard) {
        if (!props.reviewsList.comments.length && !props.reviewsList.isChecked
            || (props.reviewsList.length && props.reviewsList[0].id !== flower.flowerInfo.id)) {
            if (!props.reviewsList.checking) {
                props.getReviewsList(flower.flowerInfo.id);
            }
        }
        return (
            <Cart counter={{count, setCount}}
                  flower={flower}
                  reviewsList={props.reviewsList}
                  resultPrice={resultPrice}
                  newReview={props.newReview}
                  setFilterTitle={setFilterTitle}
                  auth={props.auth}
                  filterTitle={filterTitle}
                  changeRviewsData={props.changeRviewsData}
                  addReview={props.addReview}
                  images={images}
                  filters={filters}
                  price={price}
                  setTitles={setTitles}
                  cartStorage={cartStorage}
                  changeButtonCardWhenChangeCounts={changeButtonCardWhenChangeCounts}
                  changeButtonsCart={changeButtonsCart}
            />
        )
    } else {
        if (orderCard.orderCard) return (
            <OrderInfo flower={flower}
                       getUseFilters={getUseFilters}
                       useFilters={useFilters}
                       orderCard={orderCard}
                       auth={props.auth}
                       sendOrderData={props.sendOrderData}
                       resultPrice={resultPrice}
                       length={props.length}
                       iterator={props.iterator}
                       deleteFlowerFromCard={deleteFlowerFromCard}
                       counter={{count, setCount}}
                       setFilterTitle={setFilterTitle}
                       filterTitle={filterTitle}
                       images={images}
                       filters={filters}
                       price={price}
                       setTitles={setTitles}
                       cartStorage={cartStorage}
                       changeButtonCardWhenChangeCounts={changeButtonCardWhenChangeCounts}
                       changeButtonsCart={changeButtonsCart}/>
        );
        else return (
            <Card flower={flower}
                  resultPrice={resultPrice}
                  deleteFlowerFromCard={deleteFlowerFromCard}
                  counter={{count, setCount}}
                  setFilterTitle={setFilterTitle}
                  filterTitle={filterTitle}
                  images={images}
                  filters={filters}
                  price={price}
                  setTitles={setTitles}
                  cartStorage={cartStorage}
                  changeButtonCardWhenChangeCounts={changeButtonCardWhenChangeCounts}
                  changeButtonsCart={changeButtonsCart}/>
        )
    }

}


export default compose(
    separateDataFilters
)(CartContainer);




