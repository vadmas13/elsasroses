import React from 'react';
import styles from './../Cart.module.scss'

const Price = (props) => {
    const {price} = props;

    return (
        <div className={styles.info__price}>
            <h4>{price} ₽</h4>
        </div>
    )
}

export default Price;

