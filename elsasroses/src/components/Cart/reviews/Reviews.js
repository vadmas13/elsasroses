import React from 'react';
import styles from './../Cart.module.scss'
import {Tabs} from 'antd';
import 'antd/dist/antd.css'
import './atndReviews.scss'
import {Rate} from 'antd';
import ReviewsList from "./reviewsList/ReviewsList";


const Reviews = (props) => {
    const {TabPane} = Tabs;
    const {content, newReview, changeRviewsData, id, addReview, reviewsList, auth} = props;

    let handleSubmit = (e) => {
        if(!reviewsList.checking) {
            e.preventDefault();
            addReview(newReview, id);
        }
    }

    let handleChange = (e) => {
        let value = e.currentTarget.value;
        let field = e.target.id;
        changeRviewsData(field, value);
    }

    let changeRate = (value) => {
        changeRviewsData('rating', value);
    }

    return (

        <Tabs defaultActiveKey="1" className={styles.TabPane}>
            <TabPane tab={'Описание'} key="1">
                <div className={styles.TabPane__content}>{content}</div>
            </TabPane>
            <TabPane tab={'Отзывы'} key="2" className={styles.TabPane__tab}>
                <div className={styles.TabPane__content}>
                    <ReviewsList reviewsList={reviewsList.comments}/>
                    <h3 className={styles.TabPane__title}>Оставить отзыв</h3>
                    <form onSubmit={handleSubmit}>
                        <div className="input-field">
                            {!auth.isAuth && <label htmlFor="userName">Ваше имя</label>}
                            {
                                auth.isAuth ?
                                    <input type="text" id="userName" className={'validate'} value={auth.user.name}
                                           required/>
                                    : <input type="text" id="userName" className={'validate'} value={newReview.userName}
                                             onChange={handleChange} required/>
                            }

                        </div>
                        <div className="input-field">
                            <label htmlFor="email">E-mail</label>
                            <input type="text" id="email" className={'validate'} value={newReview.email}
                                   onChange={handleChange} required/>
                        </div>
                        <div className="input-field">
                            <label htmlFor="comment">Текст отзыва</label>
                            <textarea id="comment" onChange={handleChange} value={newReview.comment}
                                      className='materialize-textarea' required></textarea>
                        </div>
                        <div className={`${styles.TabPane__footer} input-field`}>
                            <div className={styles.TabPane__rate}>
                                <p className={styles.TabPane__rateTitle}>Ваша оценка</p>
                                <Rate allowHalf onChange={changeRate} value={newReview.rating}/>
                            </div>
                            <div className={styles.TabPane__submit}>
                                <button className={styles.button} onSubmit={handleSubmit}>
                                    {reviewsList.checking ?
                                        <div className="preloader-wrapper small active" style={{
                                            width: '20px',
                                            height: '20px'
                                        }}>
                                            <div className="spinner-layer spinner-green-only"
                                                 style={{borderColor: 'white'}}>
                                                <div className="circle-clipper left">
                                                    <div className="circle"></div>
                                                </div>
                                                <div className="gap-patch">
                                                    <div className="circle"></div>
                                                </div>
                                                <div className="circle-clipper right">
                                                    <div className="circle"></div>
                                                </div>
                                            </div>
                                        </div>
                                        : 'Оставить отзыв'}

                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </TabPane>
        </Tabs>
    )
}


export default Reviews;