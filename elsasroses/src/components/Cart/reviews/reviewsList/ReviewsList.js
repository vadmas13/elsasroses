import React from 'react';
import styles from './Reviews.module.scss'
import * as moment from 'moment';
import 'moment/locale/ru';
import {Avatar, Rate, Empty} from 'antd';



const ReviewsList = ({reviewsList}) => {
    return (
        <div className={styles.ReviewsList}>
            {reviewsList.length ? reviewsList.map((r, i) => {
                return (
                    <div className={styles.ReviewsList__item} key={i}>
                        <div className={styles.ReviewsList__header}>
                            <span className={styles.ReviewsList__name} key={`${i}_name`}>
                                <Avatar style={{backgroundColor: '#d42853', verticalAlign: 'middle', color: '#fff', fontSize: '20px'}}>
                                    { r.userName[0].toUpperCase() }
                                    </Avatar>
                                <span style={{ marginLeft: '10px'}}>{ r.userName }</span>
                                </span>
                            <span className={styles.ReviewsList__date}
                                  key={`${i}_date`}> {
                                      typeof r.date === 'string'
                                ? moment(r.date).fromNow()
                                : moment(r.date.toDate()).fromNow()}</span>
                        </div>
                        <div className={styles.ReviewsList__text}>
                            {r.comment}
                        </div>
                        <div className={styles.ReviewsList__rate}>
                            <Rate allowHalf disabled defaultValue={r.rating}/>
                        </div>
                    </div>
                )
            }) : <div className={styles.ReviewsList__empty}><Empty description={'Записей нет'}/></div>}
        </div>
    )
}


export default ReviewsList;