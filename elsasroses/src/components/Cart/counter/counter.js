import React from 'react';
import styles from './../Cart.module.scss'

const Counter = (props) => {

    const {counter, changeButtonsCart, id, added, changeButtonCardWhenChangeCounts, filterTitle} = props;

    let counterPlus = () => {
        let count = counter.count + 1;
        if(counter.count > 20) counter.setCount(20);
        else counter.setCount(count);
        changeButtonCardWhenChangeCounts(count, id);

    }
    let counterMinus = () => {
        let count = counter.count - 1;
        if(count < 1) counter.setCount(1);
        else counter.setCount(count);
        changeButtonCardWhenChangeCounts(count, id);
    }

    let addToCard = (e) => {
        let id = parseInt(e.currentTarget.id);
        changeButtonsCart(id, 'card', counter.count, true, filterTitle);
    }

    return (
        <div className={styles.info__submit + ' ' + styles.submit}>
            <div className={styles.counter}>
                <a className={styles.item} onClick={counterMinus}>
                    <i className="material-icons">remove</i>
                </a>
                <div className={styles.item + ' ' + styles.item__number}>
                    {counter && counter.count}
                </div>
                <a className={styles.item} onClick={counterPlus}>
                    <i className="material-icons">add</i>
                </a>
            </div>
            <div className={styles.submit__button}>
                { !added ?
                <button className={`${styles.addToCart}`} onClick={addToCard} id={id}>
                    <div className={`${styles.addToCart__default}`}>
                        <span>В корзину</span>
                        <i className={"small material-icons"}>add_shopping_cart</i>
                    </div>
                </button>
                    :
                    <button className={`${styles.addToCart} ${styles.addToCart__success}`} onClick={addToCard} id={id}>
                        <div className={styles.addToCart__active}>
                            <span>В корзине</span>
                            <i className={"small material-icons"}>check_circle</i>
                        </div>
                    </button>
                }
            </div>

        </div>

    )
}

export default Counter;