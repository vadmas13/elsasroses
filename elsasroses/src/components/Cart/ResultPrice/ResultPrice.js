import React from 'react';
import styles from './../Cart.module.scss'

const ResultPrice = (props) => {

    const {resultPrice} = props;


    return (
        <div className={styles.resultPrice}>
            <span className={styles.resultPrice__title}>Общая стоимость: </span>
            <span className={styles.resultPrice__price}>{ resultPrice } ₽</span>
        </div>

    )
}

export default ResultPrice;