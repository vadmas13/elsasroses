import React from 'react';
import style from "./Navbar.module.scss";
import SignedInLinks from "./Links/SignedInLinks";
import SignedOutLinks from "./Links/SignedOutLinks";


const Navbar = (props) => {
    const {compareCounts, symapthyCounts, cardCounts, isLoadedAuth, signOut, isAuth, user, authError} = props;
    const links =  isAuth && !authError ?  <SignedOutLinks user={user} signOut={signOut}/> : <SignedInLinks />

    return (
        <div className={style.navbar}>
            <nav>
                <div className="nav-wrapper">
                    <a href={'/'} className="brand-logo"><i className="material-icons">cloud</i>Logo</a>
                    <ul className="right hide-on-med-and-down">
                        {isLoadedAuth ? links : null }
                        <li><a href={'/card'}>
                            {
                                cardCounts && cardCounts !== 0 && <div className={style.countLikes}>
                                    <div className={style.countLikes__item}>{cardCounts}</div>
                                </div>
                            }

                            <i className={"small material-icons"}>add_shopping_cart</i>
                        </a></li>
                        <li><a href={'/compare'}>
                            {
                                compareCounts !== 0 && <div className={style.countLikes}>
                                    <div className={style.countLikes__item}>{compareCounts}</div>
                                </div>
                            }

                            <i className="small material-icons">shuffle</i>
                        </a></li>
                        <li><a href={'/sympathy'}>
                            {
                                symapthyCounts !== 0 && <div className={style.countLikes}>
                                    <div className={style.countLikes__item}>{symapthyCounts}</div>
                                </div>
                            }
                            <i className="small material-icons">favorite_border</i>
                        </a></li>
                    </ul>
                </div>
            </nav>

        </div>
    )
}

export default Navbar;