import React from 'react'
import {NavLink} from "react-router-dom";
import styles from './../Navbar.module.scss'


const SignedInLinks = (props) => {
    return(
        <>
            <li><NavLink to={'/signin'} className={styles.authLinks}>
                <i className={"small material-icons"}>account_circle</i>
                <span className={styles.authLinks__text}>Войти</span>
            </NavLink></li>
            <li><NavLink to={'/signup'} className={styles.authLinks}>
                <i className={"small material-icons"}>assignment_ind</i>
                <span className={styles.authLinks__text}>Зарегистрироваться</span>
            </NavLink></li>
        </>
    )
}

export default SignedInLinks;