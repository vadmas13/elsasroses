import React from 'react'
import {NavLink} from "react-router-dom";
import styles from './../Navbar.module.scss'

const SignedOutLinks = (props) => {
    const {user, signOut} = props;

    return (
        <>
            <li><a href={'/profile'} className={styles.authLinks}>
                <i className={"small material-icons"}>account_circle</i>
                <span className={styles.authLinks__text}>{ user.name }</span>
            </a></li>
            <li><a onClick={signOut} className={styles.authLinks}>
                <i className={"small material-icons"}>exit_to_app</i>
                <span className={styles.authLinks__text}>Выйти</span>
            </a></li>
        </>
    )
}

export default SignedOutLinks;