import React, {Component} from 'react';
import Navbar from "./Navbar";
import {connect} from 'react-redux'
import {firestoreConnect} from "react-redux-firebase";
import {compose} from "redux";
import {signInWithGoogle, signOut} from "../../redux/actions/authAction";

class NavbarContainer extends Component {

    componentDidMount(){

    }

    render() {
        const compareCounts = this.props.counts.compareFlowersId.length;
        const symapthyCounts = this.props.counts.sympathyFlowersId.length;
        const cardCounts = this.props.counts.cardCounts;
        const {isLoadedAuth, signOut, isAuth, user, authError} = this.props;


        return (
            <Navbar compareCounts={compareCounts} signOut={signOut}
                    isLoadedAuth={isLoadedAuth}
                    isAuth={isAuth}
                    authError={authError}
                    user={user}
                    symapthyCounts={symapthyCounts}
                    cardCounts={cardCounts}/>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        counts: state.navbar,
        isLoadedAuth: state.auth.isLoadedAuth,
        user: state.auth.user,
        isAuth: state.auth.isAuth,
        authError: state.auth.authError
    }
}


export default compose(
    connect(mapStateToProps, {signOut}),
    firestoreConnect([
        {collection: "navbar"},
    ]),
)(NavbarContainer);