import React, {useEffect, useState} from "react";
import styles from './../OrdersControl.module.scss'

const StatusOrder = ({statusOrder, status, changeStatusOrder, id}) => {

    let [colorStatus, setColorStatus ] =  useState('#444');
    let [icon, setIcon ] =  useState();

    const COLOR = 'color', ICON = 'icon' ;

    let getStatusColorIcon = (statusOrder, status, typeParam) => {
        let statusParams = statusOrder.filter(so => so.status === status);
        if(typeParam === COLOR)return statusParams[0].color;
        if(typeParam === ICON)return statusParams[0].icon;
    }

    useEffect(() => {
        setColorStatus(getStatusColorIcon(statusOrder, status, COLOR));
        setIcon(getStatusColorIcon(statusOrder, status, ICON));
    }, [status, icon])

    return(
        <div className={`input-field col s12 ${styles.StatusOrder}`}>
            <i className="small material-icons" style={{color: colorStatus}}>{icon}</i>
            <select style={{color: colorStatus}} onChange={changeStatusOrder}  id={'status_' + id}>
                {statusOrder && statusOrder.map(s => {
                    if(s.status === status){
                        return <option style={{color: s.color}} value={s.status} selected>
                            {s.status}
                        </option>
                    }
                    return <option style={{color: s.color}} value={s.status}>
                         {s.status}
                    </option>
                })
                }
            </select>
        </div>
    )
}

export default StatusOrder