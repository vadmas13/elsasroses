import React from 'react'
import styles from './OrderListSuccess.module.scss'
import moment from "moment";
import StatusOrder from "../StatusOrder/StatusOrder";
import OrderListInfo from "./OrderListInfo/OrderListInfo";
import OrderListProducts from "./OrderListProducts/OrderListProducts";


const OrderListSuccess = (props) => {
    const {trackOrder, setFocusOrder, changeStatusOrder, resultPrice, statusOrder, order} = props;

    let changeFocusOrder = (e) => {
        let id = e.currentTarget.id;
        setFocusOrder(parseInt(id));
    }

    return (
        <div className={styles.TrackOrderSuccess}>
            <OrderListInfo changeStatusOrder={changeStatusOrder}
                           statusOrder={statusOrder}
                           changeFocusOrder={changeFocusOrder}
                           setFocusOrder={setFocusOrder} trackOrder={trackOrder}
                           order={order}
            />
            <OrderListProducts trackOrder={trackOrder} resultPrice={resultPrice}/>
        </div>
    )

}

export default OrderListSuccess