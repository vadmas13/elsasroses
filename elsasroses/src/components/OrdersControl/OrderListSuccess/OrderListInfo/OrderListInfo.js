import React from 'react'
import styles from './../../../TrackOrder/TrackOrder.module.scss'
import moment from "moment";
import StatusOrder from "../../StatusOrder/StatusOrder";
import Slider from "react-slick";
import './OrderInfoSlider.scss'


const OrderListInfo = (props) => {
    const {
        trackOrder,
        changeFocusOrder,
        changeStatusOrder, order
    } = props;

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
    };

    return (
        <div className={styles.TrackOrderSuccess}>
            <div className={`${styles.TrackOrderSuccess__item} ${styles.TrackOrderSuccess__trackNumber}`}>
                <h3><span>№{trackOrder.trackNumber}</span></h3>
            </div>
            <div onClick={changeFocusOrder} id={trackOrder.trackNumber}
                 className={`${styles.TrackOrderSuccess__item} ${styles.TrackOrderSuccess__itemInfo}`}>
                <h6 className={styles.titleOrderCard}><i className="small material-icons">info_outline</i> Информация о
                    заказе</h6>
                <Slider {...settings}>
                    <div>
                        <div className={styles.TrackOrderSuccess__flowerInfo}>
                            <div className={styles.TrackOrderSuccess__params}>
                                <p className={styles.TrackOrderSuccess__paramsTitle}>Создан </p>
                                <p className={styles.TrackOrderSuccess__paramsValue}>{moment(trackOrder.createdAt.toDate()).fromNow()}</p>
                            </div>
                            <div className={styles.TrackOrderSuccess__params}>
                                <p className={styles.TrackOrderSuccess__paramsTitle}>Статус </p>
                                <StatusOrder trackNumber={trackOrder.trackNumber}
                                             id={trackOrder.id}
                                             changeStatusOrder={changeStatusOrder}
                                             status={trackOrder.status}
                                             statusOrder={props.statusOrder}/>
                            </div>
                            <div className={styles.TrackOrderSuccess__params}>
                                <p className={styles.TrackOrderSuccess__paramsTitle}>Количество</p>
                                <p className={styles.TrackOrderSuccess__paramsValue}>{order.resultCount}</p>
                            </div>
                            <div className={styles.TrackOrderSuccess__params}>
                                <p className={styles.TrackOrderSuccess__paramsTitle}>Сумма</p>
                                <p className={styles.TrackOrderSuccess__paramsValue}>{order.resultPrice} ₽</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className={styles.TrackOrderSuccess__flowerInfo}>
                            <div className={styles.TrackOrderSuccess__params}>
                                <p className={styles.TrackOrderSuccess__paramsTitle}>Телефон</p>
                                <p className={styles.TrackOrderSuccess__paramsValue}>{order.phone}</p>
                            </div>
                            <div className={styles.TrackOrderSuccess__params}>
                                <p className={styles.TrackOrderSuccess__paramsTitle}>Имя получателя</p>
                                <p className={styles.TrackOrderSuccess__paramsValue}>{order.name}</p>
                            </div>
                            <div className={styles.TrackOrderSuccess__params}>
                                <p className={styles.TrackOrderSuccess__paramsTitle}>Адрес</p>
                                <div className={styles.TrackOrderSuccess__paramsValue}>{order.address}</div>
                            </div>
                            <div className={styles.TrackOrderSuccess__params}>
                                <p className={styles.TrackOrderSuccess__paramsTitle}>Метод оплаты</p>
                                <div className={styles.TrackOrderSuccess__paramsValue}>{order.payMethod}</div>
                            </div>
                        </div>
                    </div>
                </Slider>
            </div>
        </div>
    )

}

export default OrderListInfo