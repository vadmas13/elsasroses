import React from 'react'
import styles from './../../../TrackOrder/TrackOrder.module.scss'


const OrderListProducts = (props) => {
    const {trackOrder} = props;

    return (
        <div className={`${styles.TrackOrderSuccess__item} ${!trackOrder.focus ? styles.TrackOrderSuccess__hidden : ''}`}>
            <h6 className={styles.titleOrderCard}><i className="small material-icons">local_florist</i> Товары в
                заказе</h6>
            {trackOrder.flowers.map(f => {
                return (
                    <div className={styles.TrackOrderSuccess__flowerInfo}>
                        <div className={styles.TrackOrderSuccess__params}>
                            <p className={styles.TrackOrderSuccess__paramsTitle}>Артикул</p>
                            <p className={styles.TrackOrderSuccess__paramsValue}>{f.flowerId}</p>
                        </div>
                        <div className={styles.TrackOrderSuccess__params}>
                            <p className={styles.TrackOrderSuccess__paramsTitle}>Название</p>
                            <a href={`/cart/${f.flowerId}`} className={styles.TrackOrderSuccess__titleflower}>
                                <p className={styles.TrackOrderSuccess__paramsValue}>{f.title}</p>
                            </a>
                        </div>
                        <div className={styles.TrackOrderSuccess__params}>
                            <p className={styles.TrackOrderSuccess__paramsTitle}>Количество</p>
                            <p className={styles.TrackOrderSuccess__paramsValue}>{f.count}</p>
                        </div>
                        <div className={styles.TrackOrderSuccess__params}>
                            <p className={styles.TrackOrderSuccess__paramsTitle}>Сумма</p>
                            <p className={styles.TrackOrderSuccess__paramsValue}> {props.resultPrice(f.count, f.price)} ₽</p>
                        </div>
                    </div>
                )
            })}
        </div>
    )

}

export default OrderListProducts