import React from 'react'
import styles from './OrdersControl.module.scss'
import OrdersList from "./OrdersList/OrdersList";
import FiltersTrackOrder from "./flitersTrackOrder/flitersTrackOrder";
import {getAllOrders, getStatusOrders} from "../../redux/actions/actionsGetAdminOrders";

const OrdersControl = (props) => {
    const {requestOrder} = props;

    let handleChange = (e) => {
        let value = e.target.value;
        props.getRequestOrder(value);
    }

    return (
        <div className={styles.OrderControl}>
            <div className={styles.OrderProfile}>
                <h6 className={styles.OrderControl__titleOrderCard}><i
                    className="small material-icons">playlist_add_check</i> Последние заказы</h6>
                <div className={`input-field col s6 ${styles.OrderProfile__input}`}>
                    <i className="material-icons prefix">search</i>
                    <input id="icon_prefix" type="text" className="validate" value={requestOrder}
                           onChange={handleChange}/>
                    <label htmlFor="icon_prefix">Трек номер</label>
                </div>
            </div>
            <FiltersTrackOrder statusOrder={props.statusOrder}
                               getAllOrders={props.getAllOrders}
                               getStatusOrders={props.getStatusOrders} />
            <OrdersList statusOrder={props.statusOrder}
                        changeOrderStatus={props.changeOrderStatus}
                        orders={props.orders}
                        requestOrder={requestOrder}/>
        </div>
    )
}

export default OrdersControl;