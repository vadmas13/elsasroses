import React, {useState} from 'react'
import styles from './../OrdersControl.module.scss'
import OrderListSuccess from "../OrderListSuccess/OrderListSuccess";


const OrdersList = (props) => {
    const {orders, requestOrder} = props;

    let resultPrice = (count, price) => {
        return parseInt(count) * parseInt(price.replace(' ', ''));
    }

    let getChangeOrder = (id, orders) => {
        let order = orders.filter(o => o.id === id);
        return order[0];
    }

    let changeStatusOrder = (e) => {
        let id = e.currentTarget.id.split('_');
        let status = e.currentTarget.value;
        let order = getChangeOrder(id[1], orders);
        props.changeOrderStatus(id[1], status, order);
    }

    let [focusOrder, setFocusOrder] = useState();

    let FOUND_REQUEST_ORDER = false;

    return (
        <div>

            {  orders.length !== 0 ?
                orders.map((order, i) => {
                    let trackOrder = {
                        trackNumber: order.trackNumber.toString(),
                        createdAt: order.createdAt,
                        status: order.status,
                        flowers: order.flowers,
                        focus: focusOrder === order.trackNumber,
                        id: order.id
                    }

                    if(requestOrder) {
                        if(trackOrder.trackNumber.indexOf(requestOrder) !== -1) {
                            FOUND_REQUEST_ORDER = true;
                            return <OrderListSuccess trackOrder={trackOrder}
                                                     changeStatusOrder={changeStatusOrder}
                                                     changeOrderStatus={props.changeOrderStatus}
                                                     statusOrder={props.statusOrder}
                                                     resultPrice={resultPrice}
                                                     order={order}
                                                     setFocusOrder={setFocusOrder}/>;
                        }
                        if(!FOUND_REQUEST_ORDER && orders.length === i+1) return <p>Нет совпадений</p>
                    }else return <OrderListSuccess setFocusOrder={setFocusOrder}
                                                   changeStatusOrder={changeStatusOrder}
                                                   trackOrder={trackOrder}
                                                   order={order}
                                                   changeOrderStatus={props.changeOrderStatus}
                                                   statusOrder={props.statusOrder}
                                                   resultPrice={resultPrice}/>
                }) : <div>Нет заказов с таким статусом</div>
            }
        </div>
    )
}

export default OrdersList;