import styles from "../OrdersControl.module.scss";
import React, {useState} from "react";

const FiltersTrackOrder = ({statusOrder, getStatusOrders, getAllOrders}) => {

    const ALL_ORDERS = 'all_orders';

    let [statusActive, setStatusActive] = useState(ALL_ORDERS);

    let changeRenderStatus = (e) => {
        e.preventDefault();
        let status = e.target.dataset.status;
        setStatusActive(status);
        if(status === ALL_ORDERS) getAllOrders();
        else getStatusOrders(status);
    }

    return(
        <div className={styles.filtersTrackOrder}>
            <a onClick={changeRenderStatus}
               className={statusActive === ALL_ORDERS ? styles.filtersTrackOrder__link: ''}
               data-status={ALL_ORDERS}>Все</a>
            { statusOrder.map(order => {
                return <a onClick={changeRenderStatus}
                          className={statusActive === order.status ? styles.filtersTrackOrder__link: ''}
                          data-status={order.status}>{ order.status }</a>
            })}
        </div>
    )
}

export default FiltersTrackOrder;