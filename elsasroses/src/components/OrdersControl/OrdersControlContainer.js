import React from 'react';
import {compose} from "redux";
import {getOrdersForControl} from "../../hoc/getOrdersForControl";
import OrdersControl from './OrdersControl'
import ProfileInfo from "../Profile/ProfileInfo/ProfileInfo";
import styles from './OrdersControl.module.scss'
import {getAllOrders, getStatusOrders} from "../../redux/actions/actionsGetAdminOrders";

const OrdersControlContainer = (props) => {
    const {user, orders} = props;

    return (
        <div className={styles.Profile}>
            {/*<ProfileInfo user={user}/>*/}
            <OrdersControl orders={orders}
                           getAllOrders={props.getAllOrders}
                           getStatusOrders={props.getStatusOrders}
                           changeOrderStatus={props.changeOrderStatus}
                           statusOrder={props.statusOrder}
                           requestOrder={props.requestOrder}
                           getRequestOrder={props.getRequestOrder}/>
        </div>
    )
}


export default compose(
    getOrdersForControl
)(OrdersControlContainer);