import React from 'react';
import SameProducts from "./SameProducts";
import {getSameProduct} from "../../../hoc/getSameProduct";


const SameProductsContainer = () => {
    return(
        <SameProducts />
    )
}


export default  getSameProduct(SameProductsContainer);
