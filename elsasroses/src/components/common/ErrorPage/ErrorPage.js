import React from 'react';
import styles from './ErrorPage.module.scss'

const ErrorPage = ({err}) => {
    if(!err) err = 'Нет цветов по данному запросу';
    return(
            <div className={styles.errorPage} >
                { err }
            </div>
    )
}

export default ErrorPage;