import React from 'react'
import Catalog from "./Catalog";
import getFlowersForCatalog from "../../hoc/getFlowersForCatalog";
import {compose} from "redux";

const CatalogContainer = (props) => {

    return(
        <Catalog newIdFlowerForCompare={props.newIdFlowerForCompare}
                 setInitialState={props.setInitialState}
                 catalogFilters={props.catalogFilters}
                 err={props.err}
                 badges={props.badges}
                 setInitCollapse={props.setInitCollapse}
                 defaultActiveCollapse={props.defaultActiveCollapse}
                 categories={props.categories}
                 changeUsedColors={props.changeUsedColors}
                 params={props.params}
                 newIdFlowerForSympathy={props.newIdFlowerForSympathy}
                 newIdFlowerForCard={props.newIdFlowerForCard}
            flowers={props.flowers}/>
    )
}

export default compose(getFlowersForCatalog)(CatalogContainer)