import React from 'react'
import styles from './../../Catalog.module.scss'



const ColorsFilter = (props) => {
    const {colorsState, colors} = props;

    let changeStateColors = (e) => {
        e.preventDefault()
        let name = e.target.id;
        props.changeUsedColors(name);
        let newColors = colors.filter(c => c !== name);
        if(newColors.length !== colors.length) {
            props.setColors([...newColors]);
        } else {
            props.setColors([...newColors, name]);
        }
    }

    return (
        <div className={styles.ColorsFilter}>
            {
                colorsState ? colorsState.map(color => {
                    return <a onClick={changeStateColors}
                              className={colorsState.some(c => c.name === color.name && c.use) ? styles.colorItem + ' ' + styles.colorSuccess : styles.colorItem}
                              style={{backgroundColor: color.name}} id={color.name}></a>
                }) : null
            }
        </div>
    )
}

export default ColorsFilter