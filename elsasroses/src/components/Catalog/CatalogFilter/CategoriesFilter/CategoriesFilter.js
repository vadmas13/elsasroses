import React, {useState} from 'react'
import styles from './../../Catalog.module.scss'
import {Collapse} from 'antd';
import 'antd/dist/antd.css';
import Preloader from "../../../common/Preloader/Preloader";

const {Panel} = Collapse;

const CategoriesFilter = (props) => {
    const {categories, setCategory, categoryState, defaultActiveCollapse} = props;

    let[activeCollapse, setActiveCollapse] = useState(defaultActiveCollapse);

    let handleSetCategory = (e) => {
        let id = e.currentTarget.id;
        setActiveCollapse(e.currentTarget.dataset.id);
        if(categoryState === id) setCategory('');
        else setCategory(id);
    }

    if((activeCollapse && categoryState) || !categoryState){
    return (
        <div className={styles.CategoriesFilter}>
            <Collapse defaultActiveKey={activeCollapse} accordion>
                {categories.map((c, i) => {
                    return (
                        <Panel header={c.name} key={c.id}>
                                {Object.keys(c.categories).map(category => {
                                    return (
                                        <p onClick={handleSetCategory} className={`${styles.CategoriesFilter__category} ${categoryState.trim() === category.trim() ?
                                            styles.CategoriesFilter__active : ''}`} id={category} data-id={`${c.id}`}>
                                            { categoryState.trim() === category.trim() ?
                                                <i className={`small material-icons ${styles.CategoriesFilter__icon}`}>check</i> : null }
                                                <span className={styles.CategoriesFilter__text}> {c.categories[category]} </span>
                                        </p>
                                    )
                                })}
                        </Panel>

                    )
                })}
            </Collapse>
        </div>
    )}else return <Preloader />
}

export default CategoriesFilter