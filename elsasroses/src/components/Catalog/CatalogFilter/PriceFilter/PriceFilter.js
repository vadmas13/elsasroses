import React from 'react'
import styles from './../../Catalog.module.scss'
import Nouislider from "nouislider-react";
import "nouislider/distribute/nouislider.css";
import './PriceFilter.scss'
import wNumb from 'wnumb'



const PriceFilter = (props) => {
    const {price, setPrice} = props;

    return (
        <div className={styles.PriceFilter}>
            <Nouislider
                start={[price.min, price.max]}
                connect={true}
                onEnd={(p) => setPrice({min: p[0], max: p[1]})}
                step={100}
                tooltips={[true,  true]}
                range={{
                    min: 500,
                    max: 15000
                }}
                format={ wNumb({ decimals: 0 }) }
            />
        </div>
    )
}

export default PriceFilter