import React, {useEffect, useState} from 'react'
import styles from './../Catalog.module.scss'
import {NavLink} from "react-router-dom";
import PriceFilter from "./PriceFilter/PriceFilter";
import ColorsFilter from "./ColorsFilter/ColorsFilter";
import CategoriesFilter from "./CategoriesFilter/CategoriesFilter";
import OffersFilters from "./OffersFilters/OffersFilters";


const CatalogFilter = (props) => {
    const {params} = props;

    let [price, setPrice] = useState(params.min && params.max ? {min: params.min, max: params.max} :{min: 1300, max: 6900});
    let [colors, setColors] = useState(props.catalogFilters.colors.filter(c => c.use).map(c => c.name));
    let [getParams, setGetParams] = useState();
    let [category, setCategory] = useState(params.category ? params.category : '');
    let [badgeState, setBadgeState] = useState(params.badges ? params.badges : '');

    useEffect(() => {
        let getRequestPrice = `?min=${price.min}&max=${price.max}`;
        let getRequestColors = colors.length !== 0 ? `&colors=` + colors.reduce((total, amount) => total +
            `&colors=${amount.replace('#','')}`) : '';
        let getRequestCategory = category ? `&category=${category}` : '';
        let getRequestBadges = badgeState ? `&badges=${badgeState}` : '';
        let getRequest = getRequestPrice + getRequestColors.replace('#','') + getRequestCategory + getRequestBadges;
        setGetParams(getRequest);
    }, [price, colors, category, badgeState]);

    let reloadState = () => {
        props.setInitialState();
        props.setInitCollapse();
    }


    return (
        <div className={styles.CatalogFilter}>
            <div className={styles.CatalogFilter__item}>
                <h3 className={styles.CatalogFilter__title}>По цене</h3>
                <PriceFilter price={price} setPrice={setPrice}  />
            </div>
            <div>
                <h3 className={styles.CatalogFilter__title}>Цвета</h3>
                <ColorsFilter colorsState={props.catalogFilters.colors}
                              setColors={setColors}
                              colors={colors}
                              changeUsedColors={props.changeUsedColors}/>
            </div>
            <div>
                <h3 className={styles.CatalogFilter__title}>Категории</h3>
                <CategoriesFilter categories={props.categories}
                                  defaultActiveCollapse={props.defaultActiveCollapse}
                                  categoryState={category} setCategory={setCategory}/>
            </div>
            <div>
                <h3 className={styles.CatalogFilter__title}>Наши предложения</h3>
                <OffersFilters badges={props.badges}
                               setBadgeState={setBadgeState}
                               badgeState={badgeState}/>
            </div>
            <div className={styles.CatalogFilter__button}>
                <NavLink className={styles.button} to={`/catalog/${getParams}`} onClick={reloadState}>
                    Применить фильтры
                </NavLink>
            </div>
        </div>
    )
}

export default CatalogFilter