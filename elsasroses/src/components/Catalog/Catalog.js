import React from 'react'
import styles from './Catalog.module.scss'
import Flower from "../FlowersList/Flower/Flower";
import CatalogFilter from "./CatalogFilter/CatalogFilter";
import ErrorPage from "../common/ErrorPage/ErrorPage";

const Catalog = (props) => {
    const {flowers} = props;

    return (
        <div className={styles.Catalog}>
            <div className={styles.Catalog__container}>
                <h1 className={'title'}>Каталог</h1>
                <div className={styles.Catalog__row}>
                    <div className={styles.Catalog__filter}>
                        <CatalogFilter catalogFilters={props.catalogFilters}
                                       setInitCollapse={props.setInitCollapse}
                                       params={props.params}
                                       badges={props.badges}
                                       defaultActiveCollapse={props.defaultActiveCollapse}
                                       categories={props.categories}
                                       changeUsedColors={props.changeUsedColors}
                                       setInitialState={props.setInitialState}/>
                    </div>
                    <div className={styles.Catalog__flowers}>
                        {flowers && flowers.length && !props.err ? <Flower flowers={flowers}
                                                                           newIdFlowerForCompare={props.newIdFlowerForCompare}
                                                                           newIdFlowerForSympathy={props.newIdFlowerForSympathy}
                                                                           newIdFlowerForCard={props.newIdFlowerForCard}
                        /> : <ErrorPage err={props.err}/>}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Catalog