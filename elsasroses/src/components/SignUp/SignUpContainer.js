import React, {Component} from 'react'
import {signInWithGoogle, signUp} from "../../redux/actions/authAction";
import {connect} from 'react-redux'
import {Redirect} from "react-router-dom";
import SignUp from "./SignUp";

class SignInContainer extends Component {

    state = {
        email: '',
        password: ''
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.signUp(this.state)
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    render() {
        const { authError, authorize, signInWithGoogle } = this.props;
        if(authorize.uid) return <Redirect to='/' />
        return (
            <SignUp handleSubmit={this.handleSubmit} signInWithGoogle={signInWithGoogle}
                    authError={authError} handleChange={this.handleChange}/>
        )
    }
}

const mapStateToProps = (state) => {
    return({
        authError: state.auth.authError,
        authorize: state.firebase.auth
    })
}

export default connect(mapStateToProps, {signUp, signInWithGoogle})(SignInContainer)