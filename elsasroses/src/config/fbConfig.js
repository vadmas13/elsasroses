
import firebase from "firebase/app";
import 'firebase/firestore'
import 'firebase/auth'

const firebaseConfig = {
    apiKey: "AIzaSyB9-cUlThspJtfyciI30kMpFlH-vtoCqKg",
    authDomain: "elsasroses.firebaseapp.com",
    databaseURL: "https://elsasroses.firebaseio.com",
    projectId: "elsasroses",
    storageBucket: "elsasroses.appspot.com",
    messagingSenderId: "1003977820742",
    appId: "1:1003977820742:web:144cd32c1e855a14435847",
    measurementId: "G-GKC6JME27P"
};
// Initialize Firebase




firebase.initializeApp(firebaseConfig);

export default firebase;
