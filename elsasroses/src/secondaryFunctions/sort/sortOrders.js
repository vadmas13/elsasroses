const DATE = 'date';
const STATUS = 'status';

export const ordersSortBy = (typeSort, orders) => {
    switch (typeSort) {
        case DATE: {
            return orders.sort((a, b) => {
                let date = a.createdAt.seconds - b.createdAt.seconds;
                if(date > 0) return -1;
                else if(date === 0) return 0;
                else return 1
            })
        }
        case STATUS: {
            return orders.sort((a, b) => {
                let date = a.status === 'В Обработке';
                if(date > 0) return -1;
                else if(date === 0) return 0;
                else return 1
            })
        }
        default: return orders;
    }
}