const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp(functions.config().firebase);

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const updateLastArticle = (newArticle) => {
    return admin.firestore().collection('lastArticle').doc("LA").set({
        article: newArticle
    }).then(doc => console.log(doc)).catch(err => console.log(err))
}

exports.flowerCreated = functions.firestore.document('flowers/{flower}').
onCreate(doc => {
    const project = doc.data();
    const newArticle = project.id;

    return updateLastArticle(newArticle);

})

